

 - CLASS: Circular
	 + ABSTRACT: NO
	 + IMPLEMENTS: NO
	 + EXTENDS: Shape
	 + HAS: 
	 + VARIABLES: 
		  private int x, y, r
	 + METHODS: 
		public void setX(int x) 
		public int getX() 
		public void setY(int y) 
		public int getY() 
		public void setR(int r) 
		public int getR() 
		public void draw() 
		public void rotate(int angle)
		public void moveLeft(int point) 
		public double getArea() 
		public void setColor(int color) 

 - CLASS: Client
	 + ABSTRACT: NO
	 + IMPLEMENTS: NO
	 + EXTENDS: Object
	 + HAS: 
	 + VARIABLES: 
	 + METHODS: 
		public static void main(String[] args) 

 - CLASS: Composite
	 + ABSTRACT: NO
	 + IMPLEMENTS: NO
	 + EXTENDS: Shape
	 + HAS: 
	 + VARIABLES: 
	 + METHODS: 
		public void draw() 
		public void rotate(int angle)
		public void moveLeft(int point) 
		public void setColor(int color) 
		public void add(Line shape) 
		public void remove(Line shape) 
		public Shape getChild(int id) 

 - CLASS: Line
	 + ABSTRACT: NO
	 + IMPLEMENTS: NO
	 + EXTENDS: Shape
	 + HAS: 
	 + VARIABLES: 
		  private int x1, y1, x2, y2
	 + METHODS: 
		public void setX1(int x1) 
		public int getX1() 
		public void setX2(int x2) 
		public int getX2() 
		public void setY1(int y1) 
		public int getY1() 
		public void setY2(int y2) 
		public int getY2() 
		public void draw() 
		public void rotate(int angle)
		public void moveLeft(int point) 
		public double getLength() 
		public void setColor(int color) 

 - CLASS: Shape
	 + ABSTRACT: NO
	 + IMPLEMENTS: NO
	 + EXTENDS: Object
	 + HAS: 
	 + VARIABLES: 
		  private int color
	 + METHODS: 
		public void draw() 
		public void rotate(int angle)
		public void moveLeft(int point) 
		public void setColor(int color) 
		public int getColor() 
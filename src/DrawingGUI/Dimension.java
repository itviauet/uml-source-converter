package DrawingGUI;

import java.io.Serializable;
// Dimension
public class Dimension implements Serializable {
	public int width, height;
	public Dimension(int width, int height) {
		this.height = height;
		this.width = width;
	}
}
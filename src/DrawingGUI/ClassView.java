package DrawingGUI;

import java.awt.Color;
import java.io.Serializable;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

/*
 * classView, mo ta chi tiet cac thuoc tinh cua 1 class khi duoc ve len giao dien GUI
 */
public class ClassView implements Serializable {
	private static final long serialVersionUID = 8371914083818137525L;
	public Frame tittle;
	public Vector<Frame>variables;
	public Vector<Frame>methods;
	public Point pos;
	public Dimension dim;
	public Dimension realDim;
	public boolean minimize;
	public JPanel father;
	
	public static final int KEY = 10;
	public static int SMOOTH = 100;
	
	public int MAX_LENG = 0;
	
	/*
	 * lay ra tat ca cac diem co the tren cac canh cua class
	 */
	public Vector<Point> pointCanBeChosen() {
		Vector<Point> result = new Vector<Point>();
		for(int i=pos.x+KEY; i<dim.width+pos.x-KEY; i = i + SMOOTH) {
			Point x = new Point(i, pos.y);
			Point y = new Point(i, pos.y+dim.height);
			result.add(x);
			result.add(y);
		}
		for(int i=pos.y+KEY; i<=dim.height+pos.y-KEY; i = i + SMOOTH) {
			Point x = new Point(pos.x, i);
			Point y = new Point(pos.x+dim.width, i);
			result.add(x);
			result.add(y);
		}
		return result;
	}
	// thay doi pos cua ClassView, phuc vu cho qua trinh keo tha
	public void changePos(int x, int y) {
		for(int i=0; i<variables.size(); i++) {
			variables.get(i).pos.x += x;
			variables.get(i).pos.y += y;
		}
		for(int i=0; i<methods.size(); i++) {
			methods.get(i).pos.x += x;
			methods.get(i).pos.y += y;
		}
		tittle.pos.x += x;
		tittle.pos.y += y;
	}
	// ham kiem tra xem 1 diem co toa do x, y co nam trong class hay khong
	public boolean inFrame(int x, int y) {
		if(x >= pos.x-5 && x <= pos.x+dim.width+5 && y >= pos.y-5 && y <= pos.y+dim.height+5)
			return true;
		return false;
	}
	// trang thai minimize cua classview
	public boolean inMinimizeButton(int x, int y) {
		if(x >= tittle.pos.x+tittle.dim.width-15 && x <= tittle.pos.x+tittle.dim.width-4 && y >= tittle.pos.y+5 && y <= tittle.pos.y+16)
			return true;
		return false;
	}
	// thu nho classView khi click vao button minimize
	public void doDinimize() {
		minimize = true;
		dim = tittle.dim;
		tittle.dim.width = tittle.value.length() * 5 + 60;
		for(int j=0; j<variables.size(); j++) {
			variables.get(j).label.setVisible(false);
			variables.get(j).label.setOpaque(true);
			variables.get(j).label.setForeground(Color.WHITE);
		}
		for(int j=0; j<methods.size(); j++) {
			methods.get(j).label.setVisible(false);
			methods.get(j).label.setOpaque(true);
			methods.get(j).label.setForeground(Color.WHITE);
		}
		father.repaint();
	}
	// unminimize
	public void undoMinimize() {
		minimize = false;
		dim = realDim;
		tittle.dim.width = MAX_LENG * 6;
		for(int j=0; j<variables.size(); j++) {
			variables.get(j).label.setVisible(true);
			variables.get(j).label.setOpaque(true);
			variables.get(j).label.setForeground(Color.BLACK);
		}
		for(int j=0; j<methods.size(); j++) {
			methods.get(j).label.setVisible(true);
			methods.get(j).label.setOpaque(true);
			methods.get(j).label.setForeground(Color.BLACK);
		}
		father.repaint();
	}
	public ClassView(final Frame tittle, final Vector<Frame>variables, final Vector<Frame>methods, JPanel father) {
		this.father = father;
		this.tittle = tittle;
		this.tittle.label.setHorizontalAlignment(SwingConstants.CENTER);
		this.tittle.label.setBackground(Color.WHITE);
		this.tittle.label.setForeground(Color.BLACK);
		this.variables = variables;
		this.methods = methods;
		this.pos = tittle.pos;
		
		for(int i=0; i<variables.size(); i++) 
			if(variables.get(i).value.length() > MAX_LENG) 
				MAX_LENG = variables.get(i).value.length();
		for(int i=0; i<methods.size(); i++) 
			if(methods.get(i).value.length() > MAX_LENG) 
				MAX_LENG = methods.get(i).value.length();
		
		for(int i=0; i<variables. size(); i++) {
			variables.get(i).dim.width = MAX_LENG*6;
			variables.get(i).label.setBounds(variables.get(i).pos.x, variables.get(i).pos.y, variables.get(i).dim.width, variables.get(i).dim.height);
			variables.get(i).label.setOpaque(true);	
			variables.get(i).label.setBackground(Color.CYAN);
		}
		for(int i=0; i<methods.size(); i++) {
			methods.get(i).dim.width = MAX_LENG*6;
			methods.get(i).label.setBounds(methods.get(i).pos.x, methods.get(i).pos.y, methods.get(i).dim.width, methods.get(i).dim.height);
			methods.get(i).label.setOpaque(true);	
			methods.get(i).label.setBackground(Color.CYAN);
		}
		this.dim = new Dimension(0, 0);
		this.realDim = new Dimension(0, 0);
		this.dim.width = MAX_LENG*6;
		this.dim.height = (variables.size()+methods.size()+1)*20;
		if(variables.size() != 0) this.dim.height += 3;
		if(methods.size() != 0) this.dim.height += 3;
		realDim = dim;
		tittle.dim.width = MAX_LENG*6;
		tittle.label.setBounds(tittle.pos.x, tittle.pos.y, tittle.dim.width, tittle.dim.height);
		
		minimize = false;
		
	}
	
}

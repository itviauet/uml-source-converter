package DrawingGUI;

import java.io.Serializable;
// Class Point
public class Point implements Serializable {
	public int x, y;
	public Point(){};
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public boolean isLeft(Point p) {
		if(y == p.y && x < p.x) return true;
		return false;
	}
	public boolean isRight(Point p) {
		if(y == p.y && x > p.x) return true;
		return false;
	}
	public boolean isUp(Point p) {
		if(x == p.x && y < p.y) return true;
		return false;
	}
	public boolean isDown(Point p) {
		if(x == p.x && y > p.y) return true;
		return false;
	}
}

package DrawingGUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.TextArea;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.Serializable;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import AnalysingSource.Relation;

/*
 * class de ve panel hien thi chinh cua chuong trinh
 */

public class UMLMainWindows extends JPanel implements MouseListener, MouseMotionListener, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static boolean HAS_A_RELATIONS = true;
	public static boolean IS_A_RELATIONS = true;
	public static boolean FULL_VIEW = true;
	
	public static Color BACKGROUND_COLOR = Color.WHITE;
	public static Color RELATION_HAS_A_COLOR = Color.BLACK;
	public static Color RELATION_IS_A_COLOR = Color.BLACK;
	public static Color TITTLES_FILL_COLOR = Color.WHITE;
	public static Color TITTLES_COLOR = Color.blue;
	public static Color VARIABLES_FILL_COLOR = Color.CYAN;
	public static Color VARIABLES_COLOR = Color.BLACK;
	public static Color METHODS_FILL_COLOR = Color.CYAN;
	public static Color METHODS_COLOR = Color.BLACK;
	public static Color BACKGROUND_LINE_COLOR = Color.BLUE;
	
	public static String FONT_NAME = "Cambria";
	public static int FONT_SIZE = 13;
	public static int FONT_TYPE_ABSTRACT = Font.ITALIC;
	public static int FONT_TYPE_NORMAL = Font.TRUETYPE_FONT;
	
	public Vector<ClassView> classes = new Vector<ClassView>();
	public Vector<Relation> relations = new Vector<Relation>();
	public static Vector<Integer>order = new Vector<Integer>();
	public boolean start;
	public boolean canContinue;
	public int indexRelation;
	public Point begin;
	public Point end;
	
	public static TextArea infoText;
			
	public int indexOfClass;
	
	public JPopupMenu popupMenu = new JPopupMenu();
	// Constructor
	public UMLMainWindows(){
		setSize(1040, 572);
		addMouseListener(this);
		addMouseMotionListener(this);
		JMenu x = new JMenu("Beep");
		popupMenu.add(x);
	};

	public UMLMainWindows(Vector<ClassView> classes, Vector<Relation> relations, TextArea infoText ) {
		this.infoText = infoText;
		setSize(1040, 572);
		this.classes = classes;
		this.relations = relations;
		addMouseListener(this);
		addMouseMotionListener(this);
		order = new Vector<Integer>();
		for(int i=0; i<classes.size(); i++) order.add(i);
		start = false;
		canContinue = false;
		indexOfClass = -1;
		for(int i=0; i<classes.size(); i++) {
			this.add(classes.get(i).tittle.label);
			for(int j=0; j<classes.get(i).variables.size(); j++) this.add(classes.get(i).variables.get(j).label);
			for(int j=0; j<classes.get(i).methods.size(); j++) this.add(classes.get(i).methods.get(j).label);
		}
	}
	// END Constructor
	
	public Vector<Integer> sortOrder(Vector<Integer> order, int index) {
		Vector<Integer>result = new Vector<Integer>();
		for(int i=0; i<order.size(); i++)
			if(order.get(i) != index) result.add(order.get(i));
		result.add(index);
		return result;
	}
	
	public static double distance(Point a, Point b) {
		return Math.sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
	}
	// tim duong di ngan nhat cua 2 class
	public static Point[] findRelation(ClassView a, ClassView b, Vector<ClassView>classView) {
		Point[] point = new Point[4];
		Vector<Point> apoint = a.pointCanBeChosen();
		Vector<Point> bpoint = b.pointCanBeChosen();
		double min = distance(apoint.get(0), bpoint.get(0));
		point[0] = apoint.get(0);
		point[1] = bpoint.get(0);
		point[2] = new Point(point[0].x, (point[0].y + point[1].y)/2);
		point[3] = new Point(point[1].x, (point[0].y + point[1].y)/2);
		if(a.inFrame(point[2].x, point[2].y) || a.inFrame(point[3].x, point[3].y)
		|| b.inFrame(point[3].x, point[3].y) || b.inFrame(point[2].x, point[2].y)) {
			point[2] = new Point((point[0].x + point[1].x)/2, point[0].y );
			point[3] = new Point((point[0].x + point[1].x)/2, point[1].y );
		}
		for(int i=0; i<apoint.size(); i++)
			for(int j=0; j<bpoint.size(); j++)
			if(distance(apoint.get(i), bpoint.get(j)) < min){
				min = distance(apoint.get(i), bpoint.get(j));
				point[0] = new Point(apoint.get(i).x, apoint.get(i).y);
				point[1] = new Point(bpoint.get(j).x, bpoint.get(j).y);
				point[2] = new Point(point[0].x, (point[0].y + point[1].y)/2);
				point[3] = new Point(point[1].x, (point[0].y + point[1].y)/2);
				if(a.inFrame(point[2].x, point[2].y) || a.inFrame(point[3].x, point[3].y)
				|| b.inFrame(point[3].x, point[3].y) || b.inFrame(point[2].x, point[2].y)) {
					point[2] = new Point((point[0].x + point[1].x)/2, point[0].y );
					point[3] = new Point((point[0].x + point[1].x)/2, point[1].y );
				}			
			}
		return point;
	}
	// tach key ra khoi str
	public static String cutString(String str, String key) {
		String result = "";
		int index = str.indexOf(key);
		if(index != -1) {
			for(int i=0; i<index; i++) result += str.charAt(i);
			for(int i=index+key.length()-1; i<str.length(); i++) result += str.charAt(i);
		}
		if(result == "") return str;
		return result;
	}
	// ham paint duoc override
	public void paint(Graphics g) {
		super.paint(g);
	 /// sort thu tu paint cua cac class, sao cho class dc click vao luon dc load len dau tien
		order = new Vector<Integer>();
		for(int i=0; i<classes.size(); i++) order.add(i);
		order = sortOrder(order, indexOfClass);
		
		Vector<Integer>result = new Vector<Integer>();
		for(int i=0; i<order.size(); i++)
			if(order.get(i) != indexOfClass ) result.add(order.get(i));
		result.add(indexOfClass);
		order = result;
		
		
		Font abstractFont = new Font(FONT_NAME, FONT_TYPE_ABSTRACT, FONT_SIZE);
		Font normalFont = new Font(FONT_NAME, FONT_TYPE_NORMAL, FONT_SIZE);
		
		// ve cac relations
		
		int width = 0;
		int height = 0;
		// Automatic change size preferedsize when dragging items
		if(classes != null)
		for(int i=0; i<classes.size(); i++) {
			if(classes.get(i).minimize) {
				classes.get(i).dim.width = classes.get(i).tittle.value.length() * 5 + 60;
				classes.get(i).dim.height = 20;
			} else {
				classes.get(i).dim.width = classes.get(i).MAX_LENG * 6;
				classes.get(i).dim.height = (classes.get(i).variables.size()+classes.get(i).methods.size()+1)*20;
			}
			if (width < classes.get(i).dim.width+classes.get(i).tittle.pos.x) width = classes.get(i).dim.width+classes.get(i).tittle.pos.x;
			if (height < classes.get(i).dim.height+classes.get(i).tittle.pos.y) height = classes.get(i).dim.height+classes.get(i).tittle.pos.y;
		}
		if(width < 975) width = 975;
		if(height < 531) height = 531;
		
		setPreferredSize(new java.awt.Dimension(width+10, height+10));
		setSize(new java.awt.Dimension(width+10, height+10));
		// Automatic change size and preferedsize when dragging items
		// Draw background
		
		g.setColor(BACKGROUND_COLOR);
		g.fillRect(0, 0, width+50, height+50);
		
		if(height < 544) height = 544;
		if(width < 1004) width = 1004;
		g.setColor(BACKGROUND_LINE_COLOR);
		if(height > 544) height += 10;
		if(width > 1004) width += 10;
		g.setColor(BACKGROUND_LINE_COLOR);
		for(int i=0; i<=width; i+=20) for(int j=-1; j<=height; j+=5) g.drawLine(i, j, i, j);
		for(int i=0; i<=height; i+=20) for(int j=-1; j<=width; j+=5) g.drawLine(j, i, j, i);
		// Draw background
		
		if(relations != null)
		for(int i=0; i<relations.size(); i++) {
			Point[] points = new Point[4];
			for(int j=0; j<4; j++) points[j] = new Point(0, 0);
			points = findRelation(classes.get(relations.get(i).begin), classes.get(relations.get(i).end), classes);
			relations.get(i).beginPoint = points[0];
			relations.get(i).endPoint = points[1];
			relations.get(i).breakPoint1 = points[2];
			relations.get(i).breakPoint2 = points[3];
			
			if(relations.get(i).arrow == 0) {
				g.setColor(Color.PINK);
				g.drawLine(relations.get(i).beginPoint.x, relations.get(i).beginPoint.y, relations.get(i).breakPoint1.x, relations.get(i).breakPoint1.y);
				g.drawLine(relations.get(i).breakPoint2.x, relations.get(i).breakPoint2.y, relations.get(i).breakPoint1.x, relations.get(i).breakPoint1.y);
				g.drawLine(relations.get(i).endPoint.x, relations.get(i).endPoint.y, relations.get(i).breakPoint2.x, relations.get(i).breakPoint2.y);
			}
			else
			if(relations.get(i).arrow == 1 && IS_A_RELATIONS) {
				g.setColor(RELATION_IS_A_COLOR);
				g.drawLine(relations.get(i).breakPoint2.x, relations.get(i).breakPoint2.y, relations.get(i).breakPoint1.x, relations.get(i).breakPoint1.y);
				if(relations.get(i).breakPoint2.isLeft(relations.get(i).endPoint)) {
					g.drawLine(relations.get(i).endPoint.x-3, relations.get(i).endPoint.y, relations.get(i).endPoint.x-13, relations.get(i).endPoint.y+5);
					g.drawLine(relations.get(i).endPoint.x-3, relations.get(i).endPoint.y, relations.get(i).endPoint.x-13, relations.get(i).endPoint.y-5);
					g.drawLine(relations.get(i).endPoint.x-13, relations.get(i).endPoint.y+5, relations.get(i).endPoint.x-13, relations.get(i).endPoint.y-5);
					g.drawLine(relations.get(i).breakPoint2.x, relations.get(i).breakPoint2.y, relations.get(i).endPoint.x-13, relations.get(i).endPoint.y);
				} else 
				if(relations.get(i).breakPoint2.isRight(relations.get(i).endPoint)){
					g.drawLine(relations.get(i).endPoint.x+3, relations.get(i).endPoint.y, relations.get(i).endPoint.x+13, relations.get(i).endPoint.y+5);
					g.drawLine(relations.get(i).endPoint.x+3, relations.get(i).endPoint.y, relations.get(i).endPoint.x+13, relations.get(i).endPoint.y-5);
					g.drawLine(relations.get(i).endPoint.x+13, relations.get(i).endPoint.y+5, relations.get(i).endPoint.x+13, relations.get(i).endPoint.y-5);
					g.drawLine(relations.get(i).breakPoint2.x, relations.get(i).breakPoint2.y, relations.get(i).endPoint.x+13, relations.get(i).endPoint.y);
				} else 
				if(relations.get(i).breakPoint2.isUp(relations.get(i).endPoint)) {
					g.drawLine(relations.get(i).endPoint.x, relations.get(i).endPoint.y-3, relations.get(i).endPoint.x-5, relations.get(i).endPoint.y-13);
					g.drawLine(relations.get(i).endPoint.x, relations.get(i).endPoint.y-3, relations.get(i).endPoint.x+5, relations.get(i).endPoint.y-13);
					g.drawLine(relations.get(i).endPoint.x-5, relations.get(i).endPoint.y-13, relations.get(i).endPoint.x+5, relations.get(i).endPoint.y-13);
					g.drawLine(relations.get(i).breakPoint2.x, relations.get(i).breakPoint2.y, relations.get(i).endPoint.x, relations.get(i).endPoint.y-13);
				} else 
				if(relations.get(i).breakPoint2.isDown(relations.get(i).endPoint)){
					g.drawLine(relations.get(i).endPoint.x, relations.get(i).endPoint.y+3, relations.get(i).endPoint.x-5, relations.get(i).endPoint.y+13);
					g.drawLine(relations.get(i).endPoint.x, relations.get(i).endPoint.y+3, relations.get(i).endPoint.x+5, relations.get(i).endPoint.y+13);
					g.drawLine(relations.get(i).endPoint.x-5, relations.get(i).endPoint.y+13, relations.get(i).endPoint.x+5, relations.get(i).endPoint.y+13);
					g.drawLine(relations.get(i).breakPoint2.x, relations.get(i).breakPoint2.y, relations.get(i).endPoint.x, relations.get(i).endPoint.y+13);
				}
				g.drawLine(relations.get(i).beginPoint.x, relations.get(i).beginPoint.y, relations.get(i).breakPoint1.x, relations.get(i).breakPoint1.y);
			} else
			if(relations.get(i).arrow == 2 && HAS_A_RELATIONS) {
				g.setColor(RELATION_HAS_A_COLOR);
				g.drawLine(relations.get(i).breakPoint2.x, relations.get(i).breakPoint2.y, relations.get(i).breakPoint1.x, relations.get(i).breakPoint1.y);
				if(relations.get(i).breakPoint2.isLeft(relations.get(i).endPoint)) {
					g.drawLine(relations.get(i).endPoint.x-3, relations.get(i).endPoint.y, relations.get(i).endPoint.x-13, relations.get(i).endPoint.y+5);
					g.drawLine(relations.get(i).endPoint.x-3, relations.get(i).endPoint.y, relations.get(i).endPoint.x-13, relations.get(i).endPoint.y-5);
					g.drawLine(relations.get(i).endPoint.x-13, relations.get(i).endPoint.y+5, relations.get(i).endPoint.x-13, relations.get(i).endPoint.y-5);
					g.drawLine(relations.get(i).breakPoint2.x, relations.get(i).breakPoint2.y, relations.get(i).endPoint.x-13, relations.get(i).endPoint.y);
				} else 
				if(relations.get(i).breakPoint2.isRight(relations.get(i).endPoint)){
					g.drawLine(relations.get(i).endPoint.x+3, relations.get(i).endPoint.y, relations.get(i).endPoint.x+13, relations.get(i).endPoint.y+5);
					g.drawLine(relations.get(i).endPoint.x+3, relations.get(i).endPoint.y, relations.get(i).endPoint.x+13, relations.get(i).endPoint.y-5);
					g.drawLine(relations.get(i).endPoint.x+13, relations.get(i).endPoint.y+5, relations.get(i).endPoint.x+13, relations.get(i).endPoint.y-5);
					g.drawLine(relations.get(i).breakPoint2.x, relations.get(i).breakPoint2.y, relations.get(i).endPoint.x+13, relations.get(i).endPoint.y);
				} else 
				if(relations.get(i).breakPoint2.isUp(relations.get(i).endPoint)) {
					g.drawLine(relations.get(i).endPoint.x, relations.get(i).endPoint.y-3, relations.get(i).endPoint.x-5, relations.get(i).endPoint.y-13);
					g.drawLine(relations.get(i).endPoint.x, relations.get(i).endPoint.y-3, relations.get(i).endPoint.x+5, relations.get(i).endPoint.y-13);
					g.drawLine(relations.get(i).endPoint.x-5, relations.get(i).endPoint.y-13, relations.get(i).endPoint.x+5, relations.get(i).endPoint.y-13);
					g.drawLine(relations.get(i).breakPoint2.x, relations.get(i).breakPoint2.y, relations.get(i).endPoint.x, relations.get(i).endPoint.y-13);
				} else 
				if(relations.get(i).breakPoint2.isDown(relations.get(i).endPoint)){
					g.drawLine(relations.get(i).endPoint.x, relations.get(i).endPoint.y+3, relations.get(i).endPoint.x-5, relations.get(i).endPoint.y+13);
					g.drawLine(relations.get(i).endPoint.x, relations.get(i).endPoint.y+3, relations.get(i).endPoint.x+5, relations.get(i).endPoint.y+13);
					g.drawLine(relations.get(i).endPoint.x-5, relations.get(i).endPoint.y+13, relations.get(i).endPoint.x+5, relations.get(i).endPoint.y+13);
					g.drawLine(relations.get(i).breakPoint2.x, relations.get(i).breakPoint2.y, relations.get(i).endPoint.x, relations.get(i).endPoint.y+13);
				}
				if(relations.get(i).beginPoint.isLeft(relations.get(i).breakPoint1)) {
					g.drawLine(relations.get(i).beginPoint.x+3, relations.get(i).beginPoint.y, relations.get(i).beginPoint.x+13, relations.get(i).beginPoint.y-5);
					g.drawLine(relations.get(i).beginPoint.x+3, relations.get(i).beginPoint.y, relations.get(i).beginPoint.x+13, relations.get(i).beginPoint.y+5);
					g.drawLine(relations.get(i).beginPoint.x+13, relations.get(i).beginPoint.y-5, relations.get(i).beginPoint.x+23, relations.get(i).beginPoint.y);
					g.drawLine(relations.get(i).beginPoint.x+13, relations.get(i).beginPoint.y+5, relations.get(i).beginPoint.x+23, relations.get(i).beginPoint.y);
					g.drawLine(relations.get(i).beginPoint.x+23, relations.get(i).beginPoint.y, relations.get(i).breakPoint1.x, relations.get(i).breakPoint1.y);
				} else 
				if(relations.get(i).beginPoint.isRight(relations.get(i).breakPoint1)) {
					g.drawLine(relations.get(i).beginPoint.x-3, relations.get(i).beginPoint.y, relations.get(i).beginPoint.x-13, relations.get(i).beginPoint.y-5);
					g.drawLine(relations.get(i).beginPoint.x-3, relations.get(i).beginPoint.y, relations.get(i).beginPoint.x-13, relations.get(i).beginPoint.y+5);
					g.drawLine(relations.get(i).beginPoint.x-13, relations.get(i).beginPoint.y-5, relations.get(i).beginPoint.x-23, relations.get(i).beginPoint.y);
					g.drawLine(relations.get(i).beginPoint.x-13, relations.get(i).beginPoint.y+5, relations.get(i).beginPoint.x-23, relations.get(i).beginPoint.y);
					g.drawLine(relations.get(i).beginPoint.x-23, relations.get(i).beginPoint.y, relations.get(i).breakPoint1.x, relations.get(i).breakPoint1.y);
				} else 
				if(relations.get(i).beginPoint.isUp(relations.get(i).breakPoint1)) {
					g.drawLine(relations.get(i).beginPoint.x, relations.get(i).beginPoint.y+3, relations.get(i).beginPoint.x+5, relations.get(i).beginPoint.y+13);
					g.drawLine(relations.get(i).beginPoint.x, relations.get(i).beginPoint.y+3, relations.get(i).beginPoint.x-5, relations.get(i).beginPoint.y+13);
					g.drawLine(relations.get(i).beginPoint.x+5, relations.get(i).beginPoint.y+13, relations.get(i).beginPoint.x, relations.get(i).beginPoint.y+23);
					g.drawLine(relations.get(i).beginPoint.x-5, relations.get(i).beginPoint.y+13, relations.get(i).beginPoint.x, relations.get(i).beginPoint.y+23);
					g.drawLine(relations.get(i).beginPoint.x, relations.get(i).beginPoint.y+23, relations.get(i).breakPoint1.x, relations.get(i).breakPoint1.y);
				} else 
				if(relations.get(i).beginPoint.isDown(relations.get(i).breakPoint1)) {
					g.drawLine(relations.get(i).beginPoint.x, relations.get(i).beginPoint.y-3, relations.get(i).beginPoint.x+5, relations.get(i).beginPoint.y-13);
					g.drawLine(relations.get(i).beginPoint.x, relations.get(i).beginPoint.y-3, relations.get(i).beginPoint.x-5, relations.get(i).beginPoint.y-13);
					g.drawLine(relations.get(i).beginPoint.x+5, relations.get(i).beginPoint.y-13, relations.get(i).beginPoint.x, relations.get(i).beginPoint.y-23);
					g.drawLine(relations.get(i).beginPoint.x-5, relations.get(i).beginPoint.y-13, relations.get(i).beginPoint.x, relations.get(i).beginPoint.y-23);
					g.drawLine(relations.get(i).beginPoint.x, relations.get(i).beginPoint.y-23, relations.get(i).breakPoint1.x, relations.get(i).breakPoint1.y);
				}
			}
		}		
		if(classes != null)
		for(int i=0; i<classes.size(); i++) 
		if(order.size() != 0){
			g.setColor(Color.BLACK);
			g.drawRect(	classes.get(order.get(i)).tittle.pos.x-1,
						classes.get(order.get(i)).tittle.pos.y-1,
						classes.get(order.get(i)).tittle.dim.width+2,
						classes.get(order.get(i)).tittle.dim.height+1);
			g.setColor(TITTLES_FILL_COLOR);
			g.fillRect(	classes.get(order.get(i)).tittle.pos.x,
						classes.get(order.get(i)).tittle.pos.y ,
						classes.get(order.get(i)).tittle.dim.width+1,
					classes.get(order.get(i)).tittle.dim.height);
			g.setColor(TITTLES_COLOR);
			
			String tempText2 = classes.get(order.get(i)).tittle.label.getText();
			if(tempText2.indexOf(" abstract ") != -1){
				g.setFont(abstractFont);
				tempText2 = cutString(tempText2, " abstract ");
			} else g.setFont(normalFont); 
			g.drawString(tempText2,
						classes.get(order.get(i)).tittle.pos.x+20,
						classes.get(order.get(i)).tittle.pos.y+15);
						classes.get(order.get(i)).tittle.label.setVisible(false);
			
			g.setColor(Color.BLACK);
			if(!classes.get(order.get(i)).minimize) {
				if(classes.get(order.get(i)).variables.size() != 0)
					g.drawRect(classes.get(order.get(i)).variables.get(0).pos.x-1,
					classes.get(order.get(i)).variables.get(0).pos.y-1,
					classes.get(order.get(i)).variables.get(0).dim.width+2,
					classes.get(order.get(i)).variables.get(0).dim.height*classes.get(order.get(i)).variables.size()+1);
				
				if(classes.get(order.get(i)).methods.size() != 0)
					g.drawRect(classes.get(order.get(i)).methods.get(0).pos.x-1,
					classes.get(order.get(i)).methods.get(0).pos.y-1,
					classes.get(order.get(i)).methods.get(0).dim.width+2,
					classes.get(order.get(i)).methods.get(0).dim.height*classes.get(order.get(i)).methods.size()+1);
				if(classes.size() != 0) {
					ImageIcon img = new ImageIcon("src/Images/-.bin");
					g.drawImage(img.getImage(),classes.get(order.get(i)).tittle.pos.x+classes.get(order.get(i)).tittle.dim.width-15, classes.get(order.get(i)).tittle.pos.y+5, 11, 11, null);
				}
			} else {
				if(classes.size() != 0) {
					ImageIcon img = new ImageIcon("src/Images/+.bin");
					g.drawImage(img.getImage(),classes.get(order.get(i)).tittle.pos.x+classes.get(order.get(i)).tittle.dim.width-15, classes.get(order.get(i)).tittle.pos.y+5, 11, 11, null);
				}
			}
			
			// ve cac variables
			
		for(int j=0; j<classes.get(order.get(i)).variables.size(); j++) {
				classes.get(order.get(i)).variables.get(j).label.setVisible(false);
				if(!classes.get(order.get(i)).minimize){
					g.setColor(VARIABLES_FILL_COLOR);
					g.fillRect( classes.get(order.get(i)).variables.get(j).pos.x, 
							classes.get(order.get(i)).variables.get(j).pos.y ,
							classes.get(order.get(i)).variables.get(j).dim.width+1,
							classes.get(order.get(i)).variables.get(j).dim.height);
					g.setColor(VARIABLES_COLOR);
					
					String tempText = classes.get(order.get(i)).variables.get(j).label.getText();
					tempText = cutString(tempText, " final ");
					String processText = "";
					if(tempText.indexOf("public ") != -1) { processText += " + "; tempText = cutString(tempText, "public "); } else
					if(tempText.indexOf(" private ") != -1){ processText += " - "; tempText = cutString(tempText, " private ");} else
					if(tempText.indexOf(" protected ") != -1) { processText += " # "; tempText = cutString(tempText, " protected ");}
					if(tempText.indexOf(" abstract ") != -1){
						g.setFont(abstractFont);
						tempText = cutString(tempText, " abstract ");
					}
					else g.setFont(normalFont); 
					boolean ok = false;
					if(tempText.indexOf(" static ") != -1) {
						tempText = cutString(tempText, " static ");
						ok = true;
					}
					String tempText3 = "";
					for(int k=3; k<tempText.length(); k++) tempText3 += tempText.charAt(k);
					int index2 = -1;
					for(int k=2; k<tempText3.length(); k++)
						if(tempText3.charAt(k) == ' ') {
							index2 = k;
							break;
						}
					if(index2 != -1) {
						for(int k = index2+1; k<tempText3.length(); k++) processText += tempText3.charAt(k);
						processText += " : ";
						for(int k = 0; k<index2; k++) processText += tempText3.charAt(k);
					} else processText = tempText3;
					
					g.drawString(processText,
							classes.get(order.get(i)).variables.get(j).pos.x + 20, 
							classes.get(order.get(i)).variables.get(j).pos.y + 15);	
					if(ok) g.drawLine( classes.get(order.get(i)).variables.get(j).pos.x + 34, classes.get(order.get(i)).variables.get(j).pos.y + 17,
							classes.get(order.get(i)).variables.get(j).pos.x + processText.length()*5+30,
							classes.get(order.get(i)).variables.get(j).pos.y + 17);
				}
						
			}
		// ve cac methods
			for(int j=0; j<classes.get(order.get(i)).methods.size(); j++) {
				classes.get(order.get(i)).methods.get(j).label.setVisible(false);
				if(!classes.get(order.get(i)).minimize){
					g.setColor(METHODS_FILL_COLOR);
					g.fillRect( classes.get(order.get(i)).methods.get(j).pos.x, 
							classes.get(order.get(i)).methods.get(j).pos.y ,
							classes.get(order.get(i)).methods.get(j).dim.width+1,
							classes.get(order.get(i)).methods.get(j).dim.height);
					g.setColor(METHODS_COLOR);
					
					String tempText = classes.get(order.get(i)).methods.get(j).label.getText();
					tempText = cutString(tempText, " final ");
					String processText = "";
					if(tempText.indexOf("public ") != -1) { processText += " + "; tempText = cutString(tempText, "public "); } else
					if(tempText.indexOf(" private ") != -1){ processText += " - "; tempText = cutString(tempText, " private ");} else
					if(tempText.indexOf(" protected ") != -1) { processText += " # "; tempText = cutString(tempText, " protected ");}
					if(tempText.indexOf(" abstract ") != -1){
						g.setFont(abstractFont);
						tempText = cutString(tempText, " abstract ");
					}
					else g.setFont(normalFont);
					boolean ok = false;
					if(tempText.indexOf(" static ") != -1) {
						tempText = cutString(tempText, " static ");
						ok = true;
					}
					if(tempText.indexOf(classes.get(order.get(i)).tittle.value) == 2) processText = tempText;
					else {
						String tempText3 = "";
						if(tempText.charAt(1) != ' ') tempText = "  " + tempText;
						for(int k=2; k<tempText.length(); k++) tempText3 += tempText.charAt(k);
						
							int index2 = -1;
							for(int k=0; k<tempText3.length(); k++)
								if(tempText3.charAt(k) == ' ') {
									index2 = k;
									break;
								}
							if(index2 != -1) {
								for(int k = index2+1; k<tempText3.length(); k++) processText += tempText3.charAt(k);
								processText += " : ";
								for(int k = 0; k<index2; k++) processText += tempText3.charAt(k);
							} else processText = tempText3;
						
					}
					g.drawString(processText,
						classes.get(order.get(i)).methods.get(j).pos.x + 20, 
						classes.get(order.get(i)).methods.get(j).pos.y + 15);
					
					if(ok) g.drawLine( classes.get(order.get(i)).methods.get(j).pos.x + 34, 
						classes.get(order.get(i)).methods.get(j).pos.y + 17,
						classes.get(order.get(i)).methods.get(j).pos.x + processText.length()*5+30,
						classes.get(order.get(i)).methods.get(j).pos.y + 17);
				}
			}
		}
		repaint();
	}

	
	public void updateInfoText(int i) {
		String str = "";
		str += " CLASS: " + classes.get(i).tittle.value + "\n";
		str += "     + Numbers of variables: " + classes.get(i).variables.size() + "\n";
		for(int j=0; j<classes.get(i).variables.size(); j++) 
			str += "           " + classes.get(i).variables.get(j).label.getText() + "\n";
		str += "     + Numbers of methods: " + classes.get(i).methods.size() + "\n";
		for(int j=0; j<classes.get(i).methods.size(); j++) 
			str += "             " + classes.get(i).methods.get(j).label.getText() + "\n";
		str += "     + Position:  (" + classes.get(i).tittle.pos.x + ", " + classes.get(i).tittle.pos.y + ")\n";
		str += "     + Dimension: (" + classes.get(i).dim.width + ", " + classes.get(i).dim.height + ")\n";
		if(infoText == null) infoText = new TextArea();
		infoText.setText(str);
		infoText.setCaretPosition(infoText.getText().length());
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 * bat su kien chuot de xu ly
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if(classes != null)
			for(int i=classes.size()-1; i>=0; i--) {
				if(classes.get(i).inMinimizeButton(e.getX(), e.getY())){
					if (classes.get(i).minimize) classes.get(i).undoMinimize();
					else classes.get(i).doDinimize();
					break;
				} 
			}
	}
	@Override
	public void mousePressed(MouseEvent e) {
		if(classes != null)
		for(int i=classes.size()-1; i>=0; i--) {
			if(classes.get(i).tittle.inFrame(e.getX(), e.getY())){
				updateInfoText(i);
				indexOfClass = i;
				start = true;
				order = sortOrder(order, indexOfClass);
				begin = new Point(e.getX(), e.getY());
				break;
			}
		}
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		if(start) start = false;
		canContinue = false;
	}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}
	@Override 
	public void mouseDragged(MouseEvent e) {
		if(start) {
			if(classes.get(indexOfClass).tittle.inFrame(e.getX(), e.getY())){
				updateInfoText(indexOfClass);
			}
			
			canContinue = false;
			start = true;
			end = new Point(0, 0);
			if(e.getX() > 0) end.x = e.getX();
			if(e.getY() > 0) end.y = e.getY();
			classes.get(indexOfClass).changePos(end.x - begin.x, end.y - begin.y);
			begin = end;
			revalidate();
			repaint();
		}
	}
	@Override
	public void mouseMoved(MouseEvent e) {}

}

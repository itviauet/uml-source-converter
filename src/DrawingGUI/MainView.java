package DrawingGUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import AnalysingSource.Diagram;
import AnalysingSource.Relation;
import AnalysingSource.TotalClasses;
/*
 * Man hien thi chinh cua chuong trinh
 */
public class MainView {
	public static Point pos = new Point(100, 100);
	public static Dimension dim = new Dimension(1040, 572);
	
	public static UMLMainWindows sourceToUMLPanel = new UMLMainWindows();
	
	public static JButton closeConsoleButton = new JButton();
	public static boolean closedConsole = true;
	
	public static Color FRAME_COLOR = Color.LIGHT_GRAY;
	public static Color BACK_GROUND = UMLMainWindows.BACKGROUND_COLOR;
	public static Color MENU_FOREGROUND = Color.BLACK;
	public static Color BORDER_COLOR = Color.WHITE;
	
	public static String SAVE_PATH_IMPORT_PROJECT = "src";
	public static String SAVE_PATH_EXPORT_PROJECT = "src";
	public static String SAVE_PATH_IMPORT_VDDC = "src";
	public static String SAVE_PATH_EXPORT_VDDC = "src";
	public static String SAVE_PATH_EXPORT_IMAGE = "src";
	
    public static BufferedImage createImage(JPanel panel) {
        int w = panel.getWidth();
        int h = panel.getHeight();
        BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bi.createGraphics();
        panel.paint(g);
        return bi;
    }
    // ham export image
    public static void exportImage(String URL, String name, String type, UMLMainWindows panel) {
    	BufferedImage image = createImage(panel);
		File outputfile = new File(URL + name + "." + type);
		try {
			ImageIO.write(image, type ,outputfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    // ham import project
    public static void importProject(JPanel contentPane, TextArea console) {
    	Vector<ClassView> sourceToUMLclassesView = new Vector<ClassView>();
    	Vector<Relation> sourceToUMLrelations = new Vector<Relation>();
    	JFileChooser chooserOpenProject = new JFileChooser("Open Java Project");
        chooserOpenProject = new JFileChooser("Open Java Project");
        File file = new File(SAVE_PATH_IMPORT_PROJECT);
        chooserOpenProject.setCurrentDirectory(file);
        chooserOpenProject.setAcceptAllFileFilterUsed(false);
        chooserOpenProject.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooserOpenProject.setFileHidingEnabled(false);
		int result = chooserOpenProject.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION)
		if(chooserOpenProject.getSelectedFile() != null) {
			String URL = chooserOpenProject.getSelectedFile().getAbsolutePath();
			SAVE_PATH_IMPORT_PROJECT = URL;
			Diagram diagram = new Diagram();
			TotalClasses totalClasses = diagram.getTotalClasses(URL, console);
			for(int i=0; i<totalClasses.classes.size(); i++) {
				String tittle = totalClasses.classes.get(i).name;
				if(totalClasses.classes.get(i).isAbstract) tittle = " abstract " + tittle;
			//	if(totalClasses.classes.get(i).isInterface) tittle = tittle + "<<interface>>";
				Frame tempTittle = new Frame(10, 10, 400, 20 , tittle);
				Vector<Frame>variables = new Vector<Frame>();
				Vector<Frame>methods = new Vector<Frame>();
				for(int j=0; j<totalClasses.classes.get(i).variable.size(); j++) {
					Frame tempVal = new Frame(10, 13+(j+1)*20, 400, 20, " " + totalClasses.classes.get(i).variable.get(j));
					variables.add(tempVal);
				}
				for(int j=0; j<totalClasses.classes.get(i).method.size(); j++) {
					int index;
					if(variables.size() != 0) index = 16; else index = 13;
					Frame tempMet = new Frame(10, index+(j+totalClasses.classes.get(i).variable.size()+1)*20, 400, 20, " " + totalClasses.classes.get(i).method.get(j));
					methods.add(tempMet);
				}
				ClassView Classs = new ClassView(tempTittle, variables, methods, contentPane);
				sourceToUMLclassesView.add(Classs);
			}
			for(int i=0; i<totalClasses.relation.size(); i++) sourceToUMLrelations.add(totalClasses.relation.get(i));
			sourceToUMLPanel.order = new Vector<Integer>();
			for(int i=0; i<sourceToUMLPanel.classes.size(); i++) sourceToUMLPanel.order.add(i);
			sourceToUMLPanel.classes = sourceToUMLclassesView;
			sourceToUMLPanel.relations = sourceToUMLrelations;
			sourceToUMLPanel.infoText = infoText;
			for(int i=0; i<sourceToUMLPanel.classes.size(); i++) sourceToUMLPanel.order.add(i);
			sourceToUMLPanel.repaint();
		} else System.out.println("Error Openning Project.");
    }
    // ham export file vddc
    public static void exportVDDCFile() {
    	JFileChooser fileChooser = new JFileChooser();
    	File file = new File(SAVE_PATH_EXPORT_VDDC);
    	fileChooser.setCurrentDirectory(file);
		fileChooser.setDialogTitle("Save VDDC file");   
		int userSelection = fileChooser.showSaveDialog(sourceToUMLPanel);
		String URL = null;
		if (userSelection == JFileChooser.APPROVE_OPTION) {
		    File fileToSave = fileChooser.getSelectedFile();
		    URL = fileToSave.getAbsolutePath();
		    SAVE_PATH_EXPORT_VDDC = URL;
		    try {
		    	FileOutputStream fileStream = new FileOutputStream(URL);
		    	ObjectOutputStream os = new ObjectOutputStream(fileStream);
		    	os.writeObject(sourceToUMLPanel);
		    	os.close();
		    	} catch(IOException e) {
	    		e.printStackTrace();
	    	}
			if(JOptionPane.showConfirmDialog(null, "Export VDDC File Complete !", "Export finish", JOptionPane.DEFAULT_OPTION) == JOptionPane.YES_OPTION){};
		
		}
    	
    }
    // am import file vddc
    public static void importVDDCFile(JScrollPane sourceToUML) {
    	sourceToUML.setVisible(true);
        sourceToUMLPanel.setVisible(true);
    	String URL = null;
		sourceToUMLPanel = null;
		sourceToUML.removeAll();
		JFileChooser chooserOpenProject = new JFileChooser("Open VDDC File");
		File file = new File(SAVE_PATH_IMPORT_VDDC);
		chooserOpenProject.setCurrentDirectory(file);
        chooserOpenProject.setAcceptAllFileFilterUsed(false);
        chooserOpenProject.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooserOpenProject.setFileHidingEnabled(false);
		int result = chooserOpenProject.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION) {
			if(chooserOpenProject.getSelectedFile() != null)
				URL = chooserOpenProject.getSelectedFile().getAbsolutePath();
				SAVE_PATH_IMPORT_VDDC = URL;
			try {
	    		FileInputStream fileStream = new FileInputStream(URL);
	    		ObjectInputStream os = new ObjectInputStream(fileStream);
	    		sourceToUMLPanel = (UMLMainWindows) os.readObject();
	    		os.close();
	    	} catch(Exception e) {
	    		e.printStackTrace();
	    	}
			sourceToUML.removeAll();
			sourceToUML.add(sourceToUMLPanel);
			sourceToUML.setVisible(true);
	        sourceToUMLPanel.setVisible(true);
	        welcomeText.setVisible(false);
	        welcomeScroll.setVisible(false);
		} else if (result == JFileChooser.CANCEL_OPTION) {
		    System.out.println("Cancel was selected");
		}
		sourceToUMLPanel.infoText = new TextArea();
		sourceToUML.repaint();
    	
    }
    
    public static void saveAndExit() {
    	try {
    		FileWriter writer = new FileWriter("src/Saved/Path.bin");
    		writer.write(SAVE_PATH_EXPORT_IMAGE + "\n");
    		writer.write(SAVE_PATH_EXPORT_PROJECT + "\n");
    		writer.write(SAVE_PATH_EXPORT_VDDC + "\n");
    		writer.write(SAVE_PATH_IMPORT_PROJECT + "\n");
    		writer.write(SAVE_PATH_IMPORT_VDDC + "\n");
    		writer.close();
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	
    	System.exit(0);
    }
    public static void initAndStart() {
    	try {
	    	File inFile = new File("src/Saved/Path.bin");
			FileReader fileReader = new FileReader(inFile);
			BufferedReader reader = new BufferedReader(fileReader);
			String line = null;
			String str = "";
			int index = 1;
			while((line = reader.readLine()) != null) {
				if(index == 1) SAVE_PATH_EXPORT_IMAGE = line; else
				if(index == 2) SAVE_PATH_EXPORT_PROJECT = line; else
				if(index == 3) SAVE_PATH_EXPORT_VDDC = line; else
				if(index == 4) SAVE_PATH_IMPORT_PROJECT = line; else
				if(index == 5) SAVE_PATH_IMPORT_VDDC = line;
				index++;
			}
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	//importVDDCFile("src/Saved/Old.vddc");
    }
    
    public static JFrame VFrame = new JFrame();
	public static JFrame consoleFrame = new JFrame();
    public static boolean minimizeToolBox = true;
    
    public static MyPanel contentPane = new MyPanel();	
    public static TextArea consoleText = new TextArea();
    public static JPanel welcomeText = new JPanel();
    public static JScrollPane welcomeScroll = new JScrollPane(welcomeText);
 

    public static TextArea infoText = new TextArea();
    public static JFrame infoFrame = new JFrame();
    
    public static BackgroundMenuBar menu = new BackgroundMenuBar();
    
	public void start() {
	
		sourceToUMLPanel.repaint();
		welcomeScroll.repaint();
		
		initAndStart();
		
		contentPane.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
		
		contentPane.setBackground(FRAME_COLOR);        
	    contentPane.setBounds(0, 0, dim.width, dim.height);
        consoleFrame.setUndecorated(true);
        consoleFrame.setVisible(false);
        consoleFrame.setBounds(163, 585, dim.width/2+8, 150);
        final JPanel consolePane = new JPanel();
        consolePane.setLayout(null);
        consoleFrame.setContentPane(consolePane);
        consolePane.setBackground(FRAME_COLOR);
        consolePane.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
        consoleFrame.setResizable(false);
        consoleText.setEnabled(true);
        consoleText.setEditable(false);
        consoleText.setBackground(BACK_GROUND);
        consoleText.setFont(new Font("Cambria", Font.BOLD, 10));
        consoleText.setBounds(29, 5, dim.width/2-28, 140);
        consoleFrame.add(consoleText);
        
        JButton consoleButton = new JButton(new ImageIcon("src/Images/Processing.bin"));
        consoleButton.setBounds(5, 6, 23, 138);
        consoleButton.setBorderPainted(false);
        consoleButton.setBorderPainted(true);
        consoleButton.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        consoleButton.setFocusPainted(false);
        consoleFrame.add(consoleButton);
       
        contentPane.setBackground(FRAME_COLOR);        
	    contentPane.setBounds(0, 0, dim.width, dim.height);
        infoFrame.setUndecorated(true);
        infoFrame.setVisible(false);
        infoFrame.setBounds(172+dim.width/2, 585, dim.width/2-12, 150);
        final JPanel infoPane = new JPanel();
        infoPane.setLayout(null);
        infoFrame.setContentPane(infoPane);
        infoPane.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
        infoPane.setBackground(FRAME_COLOR);
        infoFrame.setResizable(false);
        infoText.setEnabled(true);
        infoText.setEditable(false);
        infoText.setBackground(BACK_GROUND);
        infoFrame.setVisible(false);
        infoText.setFont(new Font("Cambria", Font.BOLD, 10));
        infoText.setBounds(29, 5, dim.width/2-47, 140);
        infoFrame.add(infoText);
        
        JButton infoButton = new JButton(new ImageIcon("src/Images/Properties.bin"));
        infoButton.setBounds(5, 6, 23, 138);
        infoButton.setBorderPainted(false);
        infoButton.setBorderPainted(true);
        infoButton.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        infoButton.setFocusPainted(false);
        infoFrame.add(infoButton);
        
	    final JButton iconButton = new JButton(new ImageIcon("src/Images/icon.bin"));
	    iconButton.setBackground(FRAME_COLOR);
	    iconButton.setBorderPainted(false);
	    iconButton.setBounds(7, 3, 20, 20);
	    contentPane.add(iconButton);
	    /************************************************************************************************/
	    final JButton closeButton = new JButton(new ImageIcon("src/Images/Close.bin"));
	    closeButton.setBackground(FRAME_COLOR);
	    closeButton.setFocusPainted(false);
	    closeButton.setBorderPainted(false);
	    closeButton.setBounds(dim.width-25, 2, 20, 20);
	    closeButton.setRolloverIcon(new ImageIcon("src/Images/CloseRollover.bin"));
	    closeButton.setSelectedIcon(new ImageIcon("src/Images/Close.bin"));
	    closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(JOptionPane.showConfirmDialog(null, "All data will be saved. Continue ?", "Quit", JOptionPane.OK_OPTION) == JOptionPane.YES_OPTION){
					saveAndExit();
            	} 
			}
		});
	    
	    /*
	     * Trang thai ket thuc (Save ? no)
	     * 
	     */
	    contentPane.add(closeButton);
	    /************************************************************************************************/
        final JButton minimizeButton = new JButton(new ImageIcon("src/Images/Minimize.bin"));
        minimizeButton.setBounds(dim.width-42, 3, 20, 20);
        minimizeButton.setFocusPainted(false);
        minimizeButton.setBorderPainted(false);
        minimizeButton.setRolloverIcon(new ImageIcon("src/Images/MinimizeRollover.bin"));
        minimizeButton.setSelectedIcon(new ImageIcon("src/Images/Close.bin"));
        minimizeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VFrame.setState(VFrame.ICONIFIED);
				consoleFrame.setState(consoleFrame.ICONIFIED);
				infoFrame.setState(infoFrame.ICONIFIED);
			}
		});
        minimizeButton.setBackground(FRAME_COLOR);
	    contentPane.add(minimizeButton);
	    /************************************************************************************************/
        final JButton toolbarImportProject = new JButton(new ImageIcon("src/Images/Menubar/ImportProject.bin"));
        toolbarImportProject.setBackground(FRAME_COLOR);
        toolbarImportProject.setFocusPainted(false);
        toolbarImportProject.setBorderPainted(false);
        toolbarImportProject.setBounds(6, 30, 20, 20);
        
        contentPane.add(toolbarImportProject);
        
        final JButton toolbarImportVDDC = new JButton(new ImageIcon("src/Images/Menubar/ImportVDDC.png"));
        toolbarImportVDDC.setBackground(FRAME_COLOR);
        toolbarImportVDDC.setFocusPainted(false);
        toolbarImportVDDC.setBorderPainted(false);
        toolbarImportVDDC.setBounds(6, 55, 20, 20);
       
        contentPane.add(toolbarImportVDDC);
	    /************************************************************************************************/   
        if(closedConsole) {
        	closeConsoleButton.setIcon(new ImageIcon("src/Images/ConsoleOpen.bin"));
        	closeConsoleButton.setRolloverIcon(new ImageIcon("src/Images/ConsoleOpenRollover.bin"));
        }
        else {
        	closeConsoleButton.setIcon(new ImageIcon("src/Images/ConsoleClose.bin"));
        	closeConsoleButton.setRolloverIcon(new ImageIcon("src/Images/ConsoleCloseRollover.bin"));
        }
        closeConsoleButton.setBackground(FRAME_COLOR);
        closeConsoleButton.setFocusPainted(false);
        closeConsoleButton.setBorderPainted(false);
        closeConsoleButton.setBounds(6, 547, 20, 20);
        closeConsoleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(closedConsole) {
					consoleFrame.setVisible(true);
					infoFrame.setVisible(true);
					closeConsoleButton.setIcon(new ImageIcon("src/Images/ConsoleClose.bin"));
					closeConsoleButton.setRolloverIcon(new ImageIcon("src/Images/ConsoleCloseRollover.bin"));
					closedConsole = false;
				} else {
					consoleFrame.setVisible(false);
					infoFrame.setVisible(false);
					closeConsoleButton.setIcon(new ImageIcon("src/Images/ConsoleOpen.bin"));
					closeConsoleButton.setRolloverIcon(new ImageIcon("src/Images/ConsoleOpenRollover.bin"));
					closedConsole = true;
				}
			}
		});
        contentPane.add(closeConsoleButton);
        /************************************************************************************************/
	    VFrame.setContentPane(contentPane);
	
		JButton hello = new JButton(new ImageIcon("src/Images/StartUp.bin"));
		welcomeText.setLayout(null);
		hello.setBounds(0, 1, 1002, 542);
		hello.setBackground(FRAME_COLOR);
		hello.setBorderPainted(false);
		welcomeText.add(hello);
		   
		welcomeScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		welcomeScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		welcomeScroll.setBounds(30, 22, 1004, 544);
               
	  	 
	  	 menu.BACK_GROUND = FRAME_COLOR;
		    menu.setBorderPainted(false);
		    menu.setBounds(31, 4, dim.width-37, 20);
		    try { UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());}
	        catch(Exception ex){System.out.println("Unable to load Windows look and feel");}
		
		   final JScrollPane sourceToUML = new JScrollPane(sourceToUMLPanel);
		  	
		  	iconButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {	
					welcomeScroll.setVisible(true);
					welcomeText.setVisible(true);
					sourceToUML.setVisible(false);
					sourceToUMLPanel.setVisible(false);
				}
			});
		  	
		  	sourceToUML.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		  	sourceToUML.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		  	sourceToUML.setBounds(30, 23, 1004, 544);
		  	
		  	
		  	/*
		  	 * cac menu bar
		  	 */
		  	
			    final JMenu fileMenu = new JMenu("File");
			    fileMenu.setForeground(MENU_FOREGROUND);
			    fileMenu.setBorderPainted(false); 
			    	ImageIcon iconImport = new ImageIcon("src/Images/Menubar/Import.bin");
			    	JMenu fileImport = new JMenu("Import");
			    	fileImport.setIcon(iconImport);
			    		JMenuItem fileImportProject = new JMenuItem("Java Project",new ImageIcon("src/Images/Menubar/ImportProject.bin"));
			    			fileImportProject.addActionListener(new ActionListener() {
			    				public void actionPerformed(ActionEvent e) {
			    			        importProject(sourceToUMLPanel, consoleText);
			    			        sourceToUML.setVisible(true);
			    			        sourceToUMLPanel.setVisible(true);
			    			        welcomeText.setVisible(false);
			    			        welcomeScroll.setVisible(false);
			    			        sourceToUMLPanel.repaint();
			    				}
			    			});
			    			
			    		JMenuItem fileImportVDDC = new JMenuItem("VDDC File");
			    			fileImportVDDC.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									sourceToUML.setVisible(true);
									sourceToUMLPanel.setVisible(true);
									importVDDCFile(sourceToUML);
								}
							});
			    			
			    	fileImport.add(fileImportProject);
			    	fileImport.add(fileImportVDDC);
			    	
			    	ImageIcon iconExport = new ImageIcon("src/Images/Menubar/Export.bin");
			    	JMenu fileExport = new JMenu("Export");
			    	fileExport.setIcon(iconExport);
			    		JMenuItem fileExportImage = new JMenuItem("Image");
			    		fileExportImage.addActionListener(new ActionListener() {
				            public void actionPerformed(ActionEvent event) {
				            	
				            	JFileChooser fileChooser = new JFileChooser();
				            	File file = new File(SAVE_PATH_EXPORT_IMAGE);
				            	fileChooser.setCurrentDirectory(file);
								fileChooser.setDialogTitle("Save Image File");   
								int userSelection = fileChooser.showSaveDialog(sourceToUML);
								String URL = null;
								if (userSelection == JFileChooser.APPROVE_OPTION) {
								    File fileToSave = fileChooser.getSelectedFile();
								    URL = fileToSave.getAbsolutePath();
								    SAVE_PATH_EXPORT_IMAGE = URL;
								    exportImage(URL, "", "png", sourceToUMLPanel);
								}
				                if(JOptionPane.showConfirmDialog(null, "Export Image File complete !", "Complete", JOptionPane.DEFAULT_OPTION) == JOptionPane.DEFAULT_OPTION){};
				            
				            }
				        });
			    		JMenuItem fileExportVDDC = new JMenuItem("VDDC File");
			    			fileExportVDDC.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									exportVDDCFile();
								}
							});
			    	fileExport.add(fileExportImage);
			    	fileExport.add(fileExportVDDC);
			    	
			    	JMenuItem fileExit = new JMenuItem("Exit");
			    	fileExit.addActionListener(new ActionListener() {
			            public void actionPerformed(ActionEvent event) {
			            	if(JOptionPane.showConfirmDialog(null, "All data will be saved. Continue ?", "Quit", JOptionPane.OK_OPTION) == JOptionPane.YES_OPTION){
			            		saveAndExit();
			            	} else;
			            }
			        });
			    	JMenuItem fileClean = new JMenuItem("Clean");
			    	fileClean.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							sourceToUMLPanel.classes = new Vector<ClassView>();
							sourceToUMLPanel.relations = new Vector<Relation>();
							sourceToUMLPanel.order = new Vector<Integer>();
						}
					});
			    fileMenu.add(fileImport); 
			    fileMenu.add(fileExport);
			    fileMenu.add(fileClean);
			    fileMenu.add(fileExit);
			    
			    final JMenu viewMenu = new JMenu("View");
			    viewMenu.setForeground(MENU_FOREGROUND);
			    viewMenu.setBorderPainted(false);
			    	JMenu viewMode = new JMenu("View Mode");
			    		final JCheckBoxMenuItem viewModeQuick = new JCheckBoxMenuItem("Quick", false);
			    		final JCheckBoxMenuItem viewModeFull = new JCheckBoxMenuItem("Full", true);
			    			viewModeQuick.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									viewModeQuick.setState(true);
									viewModeFull.setState(false);
									if(sourceToUMLPanel.classes != null)
									for(int i=0; i<sourceToUMLPanel.classes.size(); i++)
										sourceToUMLPanel.classes.get(i).doDinimize();
									sourceToUMLPanel.repaint();
					
								}
							});
			    			viewModeFull.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									viewModeQuick.setState(false);
									viewModeFull.setState(true);
									if(sourceToUMLPanel.classes != null)
									for(int i=0; i<sourceToUMLPanel.classes.size(); i++)
										sourceToUMLPanel.classes.get(i).undoMinimize();
									sourceToUMLPanel.repaint();
								}
							});
			    		
			    	viewMode.add(viewModeQuick);
			    	viewMode.add(viewModeFull);
			    	///////////////////////////////////////////
			    	JMenu viewRelations = new JMenu("Relations");
			    		final JCheckBoxMenuItem viewRelationsOnlyHasa = new JCheckBoxMenuItem("Has-a Relations", true);
			    		final JCheckBoxMenuItem viewRelationsOnlyIsa = new JCheckBoxMenuItem("Is-a Relations", true);
			    		final JCheckBoxMenuItem viewRelationsAll = new JCheckBoxMenuItem("All Relations", true);
			    		final JCheckBoxMenuItem viewRelationsNone = new JCheckBoxMenuItem("Nothing", false);
			    			viewRelationsOnlyHasa.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									if(viewRelationsOnlyIsa.getState() && viewRelationsOnlyHasa.getState()) {
										viewRelationsAll.setState(true);
										viewRelationsNone.setState(false);
									} else
									if(!viewRelationsOnlyIsa.getState() && !viewRelationsOnlyHasa.getState()) {
										viewRelationsAll.setState(false);
										viewRelationsNone.setState(true);
									} else {
										viewRelationsAll.setState(false);
										viewRelationsNone.setState(false);
									}
									UMLMainWindows.HAS_A_RELATIONS = viewRelationsOnlyHasa.getState();
									sourceToUMLPanel.repaint();
									UMLMainWindows.IS_A_RELATIONS = viewRelationsOnlyIsa.getState();
						
								}
							});
			    			viewRelationsOnlyIsa.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									if(viewRelationsOnlyIsa.getState() && viewRelationsOnlyHasa.getState()) {
										viewRelationsAll.setState(true);
										viewRelationsNone.setState(false);
									} else
									if(!viewRelationsOnlyIsa.getState() && !viewRelationsOnlyHasa.getState()) {
										viewRelationsAll.setState(false);
										viewRelationsNone.setState(true);
									} else {
										viewRelationsAll.setState(false);
										viewRelationsNone.setState(false);
									}
									UMLMainWindows.HAS_A_RELATIONS = viewRelationsOnlyHasa.getState();
									sourceToUMLPanel.repaint();
									UMLMainWindows.IS_A_RELATIONS = viewRelationsOnlyIsa.getState();
								}
							});
			    			viewRelationsAll.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									if(viewRelationsAll.getState()) {
										viewRelationsNone.setState(false);
										viewRelationsOnlyHasa.setState(true);
										viewRelationsOnlyIsa.setState(true);
									} else {
										if(viewRelationsOnlyIsa.getState() && viewRelationsOnlyHasa.getState()) {
											viewRelationsAll.setState(true);
											viewRelationsNone.setState(false);
										} else
										if(!viewRelationsOnlyIsa.getState() && !viewRelationsOnlyHasa.getState()) {
											viewRelationsAll.setState(false);
											viewRelationsNone.setState(true);
										}
									}
									UMLMainWindows.HAS_A_RELATIONS = viewRelationsOnlyHasa.getState();
									sourceToUMLPanel.repaint();
									UMLMainWindows.IS_A_RELATIONS = viewRelationsOnlyIsa.getState();
								}
							});
			    			viewRelationsNone.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									if(viewRelationsNone.getState()) {
										viewRelationsAll.setState(false);
										viewRelationsOnlyHasa.setState(false);
										viewRelationsOnlyIsa.setState(false);
									} 
									UMLMainWindows.HAS_A_RELATIONS = viewRelationsOnlyHasa.getState();
									sourceToUMLPanel.repaint();
									UMLMainWindows.IS_A_RELATIONS = viewRelationsOnlyIsa.getState();
								}
							});
			    	viewRelations.add(viewRelationsOnlyHasa);
			    	viewRelations.add(viewRelationsOnlyIsa);
			    	viewRelations.add(viewRelationsAll);
			    	viewRelations.add(viewRelationsNone);
			    	//////////////////////////////////////////
			    	JMenu viewSmooth = new JMenu("Smooth");
			    		JMenuItem viewSmoothHight = new JMenuItem("Height");
			    		JMenuItem viewSmoothMedium = new JMenuItem("Medium");
			    		JMenuItem viewSmoothLow = new JMenuItem("Low");
			    		JMenuItem viewSmoothAutomatic = new JMenuItem("Automatic");
			    			viewSmoothHight.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									ClassView.SMOOTH = 100;
									sourceToUMLPanel.repaint();
								}
							});
			    			viewSmoothMedium.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									ClassView.SMOOTH = 10;
									sourceToUMLPanel.repaint();
								}
							});
			    			viewSmoothLow.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									ClassView.SMOOTH = 1;
									sourceToUMLPanel.repaint();
								}
							});
			    			viewSmoothAutomatic.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									if(sourceToUMLPanel.classes.size() > 20) {
										ClassView.SMOOTH = 100;
									} else 
									if(sourceToUMLPanel.classes.size() > 5) {
										ClassView.SMOOTH = 50;
									} else {
										ClassView.SMOOTH = 1;
									} 
									sourceToUMLPanel.repaint();
								}
							});
			    	viewSmooth.add(viewSmoothHight);
			    	viewSmooth.add(viewSmoothMedium);
			    	viewSmooth.add(viewSmoothLow);
			    	viewSmooth.add(viewSmoothAutomatic);
			    	/////////////////////////////////////
			    viewMenu.add(viewMode);
			    viewMenu.add(viewRelations);
			    viewMenu.add(viewSmooth);
			    /** START COLOR MENU ***************************************************************************************************/
			    final JMenu colorMenu = new JMenu("Color");
			    colorMenu.setForeground(MENU_FOREGROUND);
			    colorMenu.setBorderPainted(false);
		    		final ColorMenu backgroundColor = new ColorMenu("Background");
		    		backgroundColor.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							    ColorMenu m = (ColorMenu) e.getSource();
							    UMLMainWindows.BACKGROUND_COLOR = m.getColor();
							    infoText.setBackground(m.getColor());
							    consoleText.setBackground(m.getColor());
						}
					});
		    		JMenu relationsColor = new JMenu("Relations Color");
			    		final ColorMenu has_a_Color = new ColorMenu("Has-a");
			    		relationsColor.add(has_a_Color);
			    		has_a_Color.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
							    ColorMenu m = (ColorMenu) e.getSource();
							    UMLMainWindows.RELATION_HAS_A_COLOR = m.getColor();
							}
						});
			    		final ColorMenu is_a_Color = new ColorMenu("Is-a");
			    		relationsColor.add(is_a_Color);
			    		is_a_Color.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								    ColorMenu m = (ColorMenu) e.getSource();
								    UMLMainWindows.RELATION_IS_A_COLOR = m.getColor();
							}
						});
			    	relationsColor.add(has_a_Color);
			    	relationsColor.add(is_a_Color);
			    	
			    	JMenu titlesColor = new JMenu("Titles Color");
			    		final ColorMenu titleFillColor = new ColorMenu("Fill Titles");
			    		titlesColor.add(titleFillColor);
			    		titleFillColor.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								    ColorMenu m = (ColorMenu) e.getSource();
								    UMLMainWindows.TITTLES_FILL_COLOR = m.getColor();
							}
						});
			    		final ColorMenu titleColor = new ColorMenu("Titles");
			    		titlesColor.add(titleColor);
			    		titleColor.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								    ColorMenu m = (ColorMenu) e.getSource();
								    UMLMainWindows.TITTLES_COLOR = m.getColor();
							}
						});
			    	titlesColor.add(titleFillColor);
			    	titlesColor.add(titleColor);
			    	
			    JMenu variablesColor = new JMenu("Variables Color");
		    		final ColorMenu variableFillColor = new ColorMenu("Fill Variables");
		    		variablesColor.add(variableFillColor);
		    		variableFillColor.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							    ColorMenu m = (ColorMenu) e.getSource();
							    UMLMainWindows.VARIABLES_FILL_COLOR = m.getColor();
						}
					});
		    		final ColorMenu variableColor = new ColorMenu("Variables");
		    		variablesColor.add(variableColor);
		    		variableColor.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							    ColorMenu m = (ColorMenu) e.getSource();
							    UMLMainWindows.VARIABLES_COLOR = m.getColor();
						}
					});
		    	variablesColor.add(variableFillColor);
		    	variablesColor.add(variableColor);
			    
			    JMenu methodsColor = new JMenu("Methods Color");
		    		final ColorMenu methodFillColor = new ColorMenu("Fill Methods");
		    		methodsColor.add(methodFillColor);
		    		methodFillColor.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							    ColorMenu m = (ColorMenu) e.getSource();
							    UMLMainWindows.METHODS_FILL_COLOR = m.getColor();
						}
					});
		    		final ColorMenu methodColor = new ColorMenu("Methods");
		    		methodsColor.add(methodColor);
		    		methodColor.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							    ColorMenu m = (ColorMenu) e.getSource();
							    UMLMainWindows.METHODS_COLOR = m.getColor();
						}
					});
		    	methodsColor.add(methodFillColor);
		    	methodsColor.add(methodColor);
		    	
			    	final ColorMenu backgroundLineColor = new ColorMenu("Background Line");
		    		backgroundLineColor.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							    ColorMenu m = (ColorMenu) e.getSource();
							    UMLMainWindows.BACKGROUND_LINE_COLOR = m.getColor();
						}
					});
		    	
		    	JMenuItem resetColor = new JMenuItem("Reset to Default");
		    	
		    	
		    	final ColorMenu frameColor = new ColorMenu("Frame Color");
	    		frameColor.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						    ColorMenu m = (ColorMenu) e.getSource();
						    FRAME_COLOR = m.getColor();
						    contentPane.setBackground(FRAME_COLOR);
						    menu.BACK_GROUND = FRAME_COLOR;
						    infoPane.setBackground(FRAME_COLOR);
						    consolePane.setBackground(FRAME_COLOR);
						    iconButton.setBackground(FRAME_COLOR);
						    toolbarImportProject.setBackground(FRAME_COLOR);
						    toolbarImportVDDC.setBackground(FRAME_COLOR);
						    minimizeButton.setBackground(FRAME_COLOR);
						    closeButton.setBackground(FRAME_COLOR);
						    closeConsoleButton.setBackground(FRAME_COLOR);
					}
				});
	    		final ColorMenu borderColor = new ColorMenu("Frame Color");
	    		frameColor.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						    ColorMenu m = (ColorMenu) e.getSource();
						    FRAME_COLOR = m.getColor();
						    contentPane.setBackground(FRAME_COLOR);
						    menu.BACK_GROUND = FRAME_COLOR;
						    infoPane.setBackground(FRAME_COLOR);
						    consolePane.setBackground(FRAME_COLOR);
						    iconButton.setBackground(FRAME_COLOR);
						    toolbarImportProject.setBackground(FRAME_COLOR);
						    toolbarImportVDDC.setBackground(FRAME_COLOR);
						    minimizeButton.setBackground(FRAME_COLOR);
						    closeButton.setBackground(FRAME_COLOR);
						    closeConsoleButton.setBackground(FRAME_COLOR);
					}
				});
	    		final ColorMenu menuForgeground = new ColorMenu("Menu Color");
	    		
		    	
		    	colorMenu.add(frameColor);
		    	colorMenu.add(menuForgeground);
		    	colorMenu.add(backgroundColor);
		    	colorMenu.add(relationsColor);
		    	colorMenu.add(titlesColor);
		    	colorMenu.add(variablesColor);
		    	colorMenu.add(methodsColor);
		    	colorMenu.add(backgroundLineColor);
		    	colorMenu.add(resetColor);
		    	
		    	final JMenu fontMenu = new JMenu("Font");
		    	fontMenu.setForeground(MENU_FOREGROUND);
		    	
		    	final JMenu settingMenu = new JMenu("Setting");
		    	settingMenu.setForeground(MENU_FOREGROUND);
		    	settingMenu.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						System.exit(0);
					}
				});
		    	
		    	final JMenu infoMenu = new JMenu("Help");
		    	infoMenu.setForeground(MENU_FOREGROUND);
		    		JMenuItem aboutInformation = new JMenuItem("About Us");
		    		ImageIcon iconInformation = new ImageIcon("src/Images/Menubar/Information.bin");
		    		aboutInformation.setIcon(iconInformation);
		    		aboutInformation.setBorderPainted(false);
		    		aboutInformation.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							VFrame.setEnabled(false);
							consoleFrame.setEnabled(false);
							final JDialog dialog = new JDialog();
							dialog.setSize(new Dimension(401, 179));
							dialog.setLayout(null);
							MyDialogPane contentPane = new MyDialogPane();
							contentPane.setLayout(null);
							JButton okButton = new JButton("OK");
							contentPane.add(okButton);
							okButton.setBounds(32, 115, 60, 20);
							okButton.setBorderPainted(false);
							okButton.setFocusPainted(false);
							okButton.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									dialog.setVisible(false);
									VFrame.setEnabled(true);
									consoleFrame.setEnabled(true);
								}
							});
							dialog.setUndecorated(true);
							dialog.setResizable(false);
							dialog.setLocationRelativeTo(null);
							dialog.setContentPane(contentPane);
							dialog.setVisible(true);
							
						}
					});
		    		
		    		JMenuItem userGuide = new JMenuItem("User Guide");
		    		userGuide.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							JFrame userFrame = new JFrame("User Guide");
							userFrame.setResizable(false);
							userFrame.setVisible(true);
							userFrame.setLayout(null);
							JTextArea txtUser = new JTextArea();
							txtUser.setEditable(false);
							txtUser.setSize(500, 500);
							txtUser.setText(" + Cách xử lý 1 Java Project ?\n    + File > Import > Java Project và chọn đường dẫn đến\n folder chứa source code \n"
									+ "+ Cách import VDDC File?\n     + File > Import > VDDC File và chọn đường\n dẫn đến file vddc cần lấy ra\n"
									+ "+ Cách xuất ra file image?\n     + Sau khi hoàn thành việc chỉnh sửa và muốn lưu lại\n hình ảnh code để sử dụng sau này, bạn có thể chọn\n export > image và chọn đường dẫn tới thư mục cần lưu\n"
									+ "+ Cách xuất ra file VDDC?\n     + Bạn đang chỉnh sửa dở và muốn lưu lại để chỉnh sửa\n tiếp vào lúc khác, bạn có thể chọn Export > VDDC File \nvà chọn đường dẫn đến thư mục cần lưu\n"
									+ "+ ");
							JScrollPane txtUserScroll = new JScrollPane(txtUser);
							userFrame.setSize(txtUser.getSize());
							userFrame.setContentPane(txtUserScroll);
							userFrame.setLocationRelativeTo(null);
						}
					});
		    		
		    	infoMenu.add(userGuide);
		    	infoMenu.add(aboutInformation);
		    	
			menu.add(fileMenu);
			menu.add(viewMenu);
			menu.add(colorMenu);
			menu.add(fontMenu);
		//	menu.add(settingMenu);
			menu.add(infoMenu);
		    
			menuForgeground.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ColorMenu m = (ColorMenu) e.getSource();
					fileMenu.setForeground(m.getColor());
					viewMenu.setForeground(m.getColor());
					colorMenu.setForeground(m.getColor());
					fontMenu.setForeground(m.getColor());
					settingMenu.setForeground(m.getColor());
					infoMenu.setForeground(m.getColor());
				}
			});
			resetColor.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					UMLMainWindows.BACKGROUND_COLOR = Color.WHITE;
					UMLMainWindows.RELATION_HAS_A_COLOR = Color.BLACK;
					UMLMainWindows.RELATION_IS_A_COLOR = Color.BLACK;
					UMLMainWindows.TITTLES_FILL_COLOR = Color.WHITE;
					UMLMainWindows.TITTLES_COLOR = Color.blue;
					UMLMainWindows.VARIABLES_FILL_COLOR = Color.CYAN;
					UMLMainWindows.VARIABLES_COLOR = Color.BLACK;
					UMLMainWindows.METHODS_FILL_COLOR = Color.CYAN;
					UMLMainWindows.METHODS_COLOR = Color.BLACK;
					UMLMainWindows.BACKGROUND_LINE_COLOR = Color.BLUE;
					FRAME_COLOR = Color.LIGHT_GRAY;
					contentPane.setBackground(FRAME_COLOR);
				    menu.BACK_GROUND = FRAME_COLOR;
				    infoPane.setBackground(FRAME_COLOR);
				    consolePane.setBackground(FRAME_COLOR);
				    iconButton.setBackground(FRAME_COLOR);
				    toolbarImportProject.setBackground(FRAME_COLOR);
				    toolbarImportVDDC.setBackground(FRAME_COLOR);
				    minimizeButton.setBackground(FRAME_COLOR);
				    closeButton.setBackground(FRAME_COLOR);
				    closeConsoleButton.setBackground(FRAME_COLOR);
				    BACK_GROUND = Color.WHITE;
				    infoText.setBackground(BACK_GROUND);
				    consoleText.setBackground(BACK_GROUND);
				    MENU_FOREGROUND = Color.BLACK;
				    fileMenu.setForeground(MENU_FOREGROUND);
					viewMenu.setForeground(MENU_FOREGROUND);
					colorMenu.setForeground(MENU_FOREGROUND);
					fontMenu.setForeground(MENU_FOREGROUND);
					settingMenu.setForeground(MENU_FOREGROUND);
					infoMenu.setForeground(MENU_FOREGROUND);
				}
			});
			
		VFrame.add(menu);
		
        contentPane.add(welcomeScroll);
        contentPane.add(sourceToUML);
        welcomeScroll.setVisible(true);
        sourceToUML.setVisible(false);
           
       hello.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {		
				welcomeScroll.setVisible(false);
				sourceToUML.setVisible(true);	
				sourceToUMLPanel.setVisible(true);
				sourceToUMLPanel.repaint();
			}
		});

		toolbarImportProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				importProject(sourceToUMLPanel, consoleText);
				sourceToUML.setVisible(true);
		        sourceToUMLPanel.setVisible(true);
		        welcomeText.setVisible(false);
		        welcomeScroll.setVisible(false);
				sourceToUMLPanel.repaint();
			}
		});
		
		 toolbarImportVDDC.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					importVDDCFile(sourceToUML);
				}
			});
		
       	VFrame.setUndecorated(true);
		VFrame.setLayout(null);
		VFrame.setBounds(pos.x, pos.y, dim.width, dim.height);
		VFrame.setVisible(true);
		VFrame.setBounds(163, 10, dim.width, dim.height);
	}
	
}

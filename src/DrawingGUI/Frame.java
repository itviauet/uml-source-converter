package DrawingGUI;

import java.io.Serializable;

import javax.swing.JLabel;
// la class chua 1 khung hinh nho, de luu toa do chi tiet cua cac variable, methods
public class Frame implements Serializable {
	public Point pos;
	public Dimension dim;
	public String value;
	JLabel label;
	public boolean inFrame(int x, int y) {
		if(x >= pos.x && x <= pos.x+dim.width && y >= pos.y && y <= pos.y+dim.height)
			return true;
		return false;
	}
	Frame(int x, int y, int width, int height, String value) {
		label = new JLabel(value);
	//	label.setOpaque(true);	
	//	label.setBackground(Color.CYAN);
	//	label.setForeground(Color.BLACK);
	//	label.setFont(new java.awt.Font("Serif", 0, 10));
		label.setBounds(0, 0, 50, 20);
		this.value = value;
		pos = new Point(x, y);
		dim = new Dimension(width, height);
	}
}

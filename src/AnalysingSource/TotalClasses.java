package AnalysingSource;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

/*
 * chua tat ca cac class
 */
public class TotalClasses {
	public Vector<Class> classes;
	public Vector<Relation> relation;
	
	public String getClass(int index) {
		return classes.get(index).name;
	}

	public TotalClasses() {
		classes = new Vector<Class>();
		relation = new Vector<Relation>();
	}
	public void print(String fileName) {
		try {
			FileWriter writer = new FileWriter(fileName);
			for(int i=0; i<classes.size(); i++)
				classes.get(i).print(writer);
			writer.close();			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
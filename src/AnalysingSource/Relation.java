package AnalysingSource;

import java.io.Serializable;

import DrawingGUI.Point;
/*
 * chua moi quan he cua cac class
 */
public class Relation implements Serializable {
	public int begin;
	public int end;
	public Point beginPoint;
	public Point breakPoint1;
	public Point breakPoint2;
	public Point endPoint;
	public int arrow;
	public boolean inBreakPoint(Point breakPoint, int x, int y) {
		if(x > breakPoint.x-10 && x < breakPoint.x+10 && y > breakPoint.y-10 && y < breakPoint.y+10)
			return true;
		return false;
	}
	public Relation(int begin, int end, int arrow) {
		breakPoint1 = new Point(0, 0);
		breakPoint2 = new Point(0, 0);
		beginPoint = new Point(0, 0);
		endPoint = new Point(0, 0);
		this.begin = begin;
		this.end = end;
		this.arrow = arrow;
	}
}

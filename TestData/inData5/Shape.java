public class Shape {
	private int color;
	// Constructor
	Shape() {
		color = 0;
	}
	// Copy Constructor
	Shape(Shape shape) {
		this.color = shape.color;
	}
	public void draw() {
		System.out.println(" - Bat dau ve hinh");
	};
	
	
	public void rotate(int angle){
		System.out.println(" - Xoay hinh " + angle + " do");
	};
	public void moveLeft(int point) {
		System.out.println("- Move left");
	};
	public void setColor(int color) {
		this.color = color;
	}
	public int getColor() {
		return this.color;
	}

}

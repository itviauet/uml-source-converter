public class Composite extends Shape {
	public Line[] line = new Line[100];
	public Circular circular = new Circular();
	// Constructor
	public Composite() {
	}
	// Copy constructor
	public Composite(Line[] line, Circular circular) {
		for(int i=0; i<3; i++) this.line[i] = new Line(line[i]);
		this.circular = new Circular(circular);
	}
	public void draw() {
		super.draw();
		System.out.println(" + Ve hinh Composite");
		for(int i=0; i<3; i++) line[i].draw();
		circular.draw();
	}
	public void rotate(int angle){
		super.rotate(angle);
		System.out.println(" + Xoay hinh Composite " + angle + " do");
		for(int i=0; i<3; i++) line[i].rotate(angle);
		circular.rotate(angle);
	}
	public void moveLeft(int point) {
		super.moveLeft(point);
		System.out.println("+ Move left Composite");
	}
	public void setColor(int color) {
		super.setColor(color);
	}
	public void add(Line shape) {
		Line[] temp = new Line[100];
		for(int i=0; i<3; i++) temp[i] = new Line(this.line[i]);
		temp[4] = new Line(shape);
	}
	public void remove(Line shape) {
		this.line[3] = null;
	}
	public Shape getChild(int id) {
		if(0<=id && id <=2) return line[id]; else
		if(id == 3) return circular; else 
		System.out.println(" Cannot getChild ! Id ERROR !");
		return new Shape();
	}

}
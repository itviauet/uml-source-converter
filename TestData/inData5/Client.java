public class Client {

	public static void main(String[] args) {
		
		// Khai bao cac bien va khoi tao gia tri bang constructor
		Line[] line = new Line[3];
		Circular circular = new Circular(200, 400, 100, 0);
		
		// Khai bao 1 mang figure va khoi tao gia tri bang constructor
		Shape[] figure = new Shape[4];
		for(int i=0; i<3; i++) line[i] = new Line(50*i, 100, 50*i, 150, 0);
		figure[0] = new Line(50, 10, 50, 60, 0); 
		figure[1] = new Circular(100, 100, 50, 0);
		figure[2] = new Circular(200, 200, 100, 0);
		figure[3] = new Composite(line, circular);
		
		// Mo phong thao tac draw() va rotate()
		for(int i=0; i<=3; i++) figure[i].draw();
		for(int i=0; i<=3; i++) figure[i].rotate(90);

	}

}

public class Circular extends Shape {
	private int x, y, r;
	// Constructor 
	Circular() {
	}
	// Copy Constructor
	Circular(Circular circular) {
		this.x = circular.x;
		this.y = circular.y;
		this.r = circular.r;
		this.setColor(circular.getColor());
	}
	// Construcor with x, y, r and color
	Circular(int x, int y, int r, int color) {
		this.x = x;
		this.y = y;
		this.r = r;
		this.setColor(color);
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getX() {
		return this.x;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getY() {
		return this.y;
	}
	public void setR(int r) {
		this.r = r;
	}
	public int getR() {
		return this.r;
	}
	public void draw() {
		super.draw();
		System.out.println("Ve hinh tron");
	}
	public void rotate(int angle){
		super.rotate(angle);
		System.out.println("Xoay hinh tron " + angle + " do");
	}
	public void moveLeft(int point) {
		super.moveLeft(point);
		System.out.println("Move left hinh tron");
	}
	public double getArea() {
		return 3.14*r*r;
	}
	public void setColor(int color) {
		super.setColor(color);
	}
}


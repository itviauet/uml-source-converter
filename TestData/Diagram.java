import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Vector;

public class Diagram {
	public Vector<String> KEYWORDS;
	Diagram() {
		KEYWORDS = new Vector<String>();
		KEYWORDS.add("public");
		KEYWORDS.add("private");
		KEYWORDS.add("protected");
		KEYWORDS.add("class");
	}
	public boolean hasKeywords(String str, int index, String keyword) {
		int count = 0;
		if(str.length() < keyword.length()) return false;
		for(int i=index; i<index+keyword.length(); i++)
			if(i<str.length() && count<keyword.length() && str.charAt(i) != keyword.charAt(count++)) return false;
		return true;
	}
	public boolean hasKeywords(String str, int index) {
		for(int j=0; j<KEYWORDS.size(); j++) {
			if(hasKeywords(str, index, KEYWORDS.get(j))) return true;
		}
		return false;
	}
	public String standardized(String str) {
		String temp = "";
		for(int i=0; i<str.length(); i++) {
			if(str.charAt(i) == ' ' && i>0 && str.charAt(i-1) == ' ');
				else temp += str.charAt(i);
		}
		String result = "";
		int i = 0;
		while(i < temp.length()-1) {
			if(temp.charAt(i) == '/' && i < temp.length()-1 && temp.charAt(i+1) == '*') {
				i += 2;
				while(i<temp.length()-1 && temp.charAt(i) != '*' && temp.charAt(i+1) != '/') i++;
				i += 2;
			}
			if(i<temp.length()) result += temp.charAt(i);
			i++;
		}
		return result;
	}
	public boolean hasBracket(String str) {
		for(int i=0; i<str.length(); i++)
			if(str.charAt(i) == '(') return true;
		return false;
	}
	public Class process(String fileDir, String fileName) {
		Class Classes = new Class();
		try {
			File inFile = new File(fileDir + '/' + fileName);
			FileReader fileReader = new FileReader(inFile);
			BufferedReader reader = new BufferedReader(fileReader);
			String line = null;
			String str = "";
			while((line = reader.readLine()) != null) {
				for(int i=0; i<line.length(); i++) {
					if(line.charAt(i) == '/' && i<line.length()-1)
						if (line.charAt(i+1) == '/') break;
					str += line.charAt(i);
				}
			}
			
			str = standardized(str);
			
			String str2 = "";
			int i = 0;
			int xx = 0;
			while(i < str.length()) {
				str2 += str.charAt(i);
				if(str.charAt(i) == '{') {
					xx ++;
					if(xx != 1)
					while(i<str.length() && str.charAt(i) != '}') i++;
				}
				i++;
			}

			Vector<String>arrString = new Vector<String>();
			i = 0;
			while(i < str2.length()) {
				if(hasKeywords(str2, i)) {
					String temp = "";
					while(str2.charAt(i) != '{' && str2.charAt(i) != ';' && str2.charAt(i) != '=' && i < str2.length()-1) {
						temp += str2.charAt(i++);
					}
					arrString.add(temp);
				}
				i++;
			}
			
			i = 0;
			while(arrString.size() != 0 && i < arrString.get(0).length()) {
				if (hasKeywords(arrString.get(0), i, "class")) {
					while(i<arrString.get(0).length() && arrString.get(0).charAt(i) != ' ') i++;
					i++;
					while(i<arrString.get(0).length() && arrString.get(0).charAt(i) != ' ') {
						Classes.name += arrString.get(0).charAt(i++);
					}
					break;
				}
				i++;
			}
			i = 0;
			while(arrString.size() != 0 && i < arrString.get(0).length()) {
				if (hasKeywords(arrString.get(0), i, "extends")) {
					while(i<arrString.get(0).length() && arrString.get(0).charAt(i) != ' ') i++;
					i++;
					Classes.isExtends = "";
					while(i<arrString.get(0).length() && arrString.get(0).charAt(i) != ' ' && arrString.get(0).charAt(i) != '{') {
						Classes.isExtends += arrString.get(0).charAt(i++);
					}
					break;
				}
				i++;
			}
			i = 0;
			while(arrString.size() != 0 && i < arrString.get(0).length()) {
				if (hasKeywords(arrString.get(0), i, "implements")) {
					while(i<arrString.get(0).length() && arrString.get(0).charAt(i) != '{' && arrString.get(0).charAt(i) != ' ') i++;
					i++;
					String temp = "";
					while(i<arrString.get(0).length() && arrString.get(0).charAt(i) != '{') {
						if(arrString.get(0).charAt(i) != ' ') temp += arrString.get(0).charAt(i++);
						else i++;
					}
					int j=0;
					while(j<temp.length()) {
						String x = "";
						while(j<temp.length() && temp.charAt(j) != ',') x += temp.charAt(j++);
						j++;
						Classes.isImplements.add(x);
					}
					break;
				}
				i++;
			}
			if(arrString.size() != 0)
			for(int j=0; j<arrString.get(0).length(); j++)
				if(hasKeywords(arrString.get(0), j, "abstract"))
					Classes.isAbstract = true;
			
			String temp = "";
			for(int k=0; k<str.length(); k++)
				if(hasKeywords(str, k, Classes.name + '(') && (str.charAt(k-2) == ';' || str.charAt(k-1) == ';')) {
					int l = k;
					while(str.charAt(l-1) != ')') {
						temp += str.charAt(l++);
					}
				}
			if(temp != "") Classes.function.add(temp);
			
			for(int j=1; j<arrString.size(); j++) {
				if(!hasBracket(arrString.get(j))) Classes.variable.add(arrString.get(j));
				else Classes.function.add(arrString.get(j));
			}
			
			reader.close();
	
			
			System.out.println("\tComplete analysing file: " + fileName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Classes;
	}
	public void diagraming(TotalClasses totalClasses, String fileDir) {
		File dir = new File(fileDir);
		dir.mkdir();
		if(dir.isDirectory()) {
			System.out.println(" + START PROCESS FOLDER: " + fileDir);
			String[] dirContents = dir.list();
			for(int i=0; i<dirContents.length; i++) {
				String s = dirContents[i];
				if (hasKeywords(s, s.length()-5, ".java")) totalClasses.classes.add(process(fileDir, dirContents[i]));
				else {
					s = fileDir + '/' + s;
					diagraming(totalClasses, s);
				}
			}
		}
	}
	public TotalClasses getTotalClasses(String fileDir) {
		TotalClasses totalClasses = new TotalClasses();
		diagraming(totalClasses, fileDir);
		for(int i=0; i<totalClasses.classes.size(); i++)
			for(int j=0; j<totalClasses.classes.size(); j++) 
				if(i != j) {
					for(int k=0; k<totalClasses.classes.get(i).variable.size(); k++)
						if(totalClasses.classes.get(i).variable.get(k).indexOf(" " + totalClasses.classes.get(j).name + " ") != -1 
						|| totalClasses.classes.get(i).variable.get(k).indexOf("<" + totalClasses.classes.get(j).name + ">") != -1  
						|| totalClasses.classes.get(i).variable.get(k).indexOf(" " + totalClasses.classes.get(j).name + "[]") != -1 ) {
							totalClasses.classes.get(i).has_a.add(totalClasses.classes.get(j).name);
							Relation re = new Relation(i, j, 2);
							totalClasses.relation.add(re);
						}
					if(totalClasses.classes.get(i).isExtends.indexOf(totalClasses.classes.get(j).name) == 0) {
						Relation re = new Relation(i, j, 1);
						totalClasses.relation.add(re);
					}
							
				}
		totalClasses.print("Result.txt");
		return totalClasses;
	}
}


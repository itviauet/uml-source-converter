public class Relation {
	public int begin;
	public int end;
	public int arrow;
	public Relation(int begin, int end, int arrow) {
		this.begin = begin;
		this.end = end;
		this.arrow = arrow;
	}
}

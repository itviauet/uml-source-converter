public class TemperatureException extends Exception {
	public TemperatureException() {
		super();
	}
	public TemperatureException(String str) {
		super(str);
	}
}

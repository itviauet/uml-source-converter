import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;


public class TotalClasses {
	public Vector<Class> classes;
	public Vector<Relation> relation;
	
	
	public String getClass(int index) {
		return classes.get(index).name;
	}

	
	public TotalClasses() {
		classes = new Vector<Class>();
		relation = new Vector<Relation>();
	}
	public void print(String fileName) {
		try {
			FileWriter writer = new FileWriter(fileName);
			writer.write("\n   Class DIAGRAM - GROUP 20 - VIET ANH, DUNG RINH, KHANH CHI, BA DAO ");
			for(int i=0; i<classes.size(); i++)
				classes.get(i).print(writer);
			writer.close();
			
			for(int i=0; i<relation.size(); i++) 
				if(relation.get(i).arrow == 1) System.out.println(getClass(relation.get(i).begin) + " EXTEND : " + getClass(relation.get(i).end));
				else
				if(relation.get(i).arrow == 2) System.out.println(getClass(relation.get(i).begin) + " HAS : " + getClass(relation.get(i).end));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
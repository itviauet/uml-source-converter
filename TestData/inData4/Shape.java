public class Shape {
	private int color;
	private int x1, y1, x2, y2, x, y, r;
	public Line[] line = new Line[3];
	public Circular circular;
	Shape() {
		color = 0;
	}
	Shape(Shape shape) {
		this.color = shape.color;
	}
	public void draw() {
		System.out.println(" - Bat dau ve hinh");
	};
	public void setX1(int x1) {
		this.x1 = x1;
	}
	public int getX1() {
		return this.x1;
	}
	public void setX2(int x2) {
		this.x2 = x2;
	}
	public int getX2() {
		return this.x2;
	}
	public void setY1(int y1) {
		this.y1 = y1;
	}
	public int getY1() {
		return this.y1;
	}
	public void setY2(int y2) {
		this.y2 = y2;
	}
	public int getY2() {
		return this.y2;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getX() {
		return this.x;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getY() {
		return this.y;
	}
	public void setR(int r) {
		this.r = r;
	}
	public int getR() {
		return this.r;
	}
	public void rotate(int angle){
		System.out.println(" - Xoay hinh " + angle + " do");
	};
	public void moveLeft(int point) {
		System.out.println("Move left");
	};
	public void setColor(int color) {
		this.color = color;
	}
	public int getColor() {
		return this.color;
	}

}

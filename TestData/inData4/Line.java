public class Line extends Shape {
	private int x1, y1, x2, y2;
	
	Line() {
	}
	Line(int x1, int y1, int x2, int y2, int color) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.setColor(color);
	}
	Line(Line x) {
		this.x1 = x.x1;
		this.x2 = x.x2;
		this.y1 = x.y1;
		this.y2 = x.y2;
		this.setColor(x.getColor());
	}

	public void draw() {
		super.draw();
		System.out.println("Ve duong thang");
	}
	public void rotate(int angle){
		super.rotate(angle);
		System.out.println("Xoay duong thang " + angle + " do");
	}
	public void moveLeft(int point) {
		super.moveLeft(point);
		System.out.println("Move left duong thang");
	}
	public double getLength() {
		return Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
	}
	public void setColor(int color) {
		super.setColor(color);
	}
}
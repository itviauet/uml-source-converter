import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Client extends JPanel {
	private static final long serialVersionUID = 1L;
	
	public static Line[] line = new Line[3];
	public static Circular circular = new Circular(200, 400, 100, 0);
	public static Shape[] figure = new Shape[4];
	

	public void paint(Graphics g) {
		
		super.paint(g);
		g.setColor(Color.YELLOW);
		if(figure[0].getColor() == 0) g.setColor(Color.YELLOW); else
		if(figure[0].getColor() == 1) g.setColor(Color.RED); else
		g.setColor(Color.YELLOW);
		
		System.out.println(figure[0].getX1() + figure[0].getY1() + figure[0].getX2() + figure[0].getY2());
		
		g.drawLine(figure[0].getX1(), figure[0].getY1(), figure[0].getX2(), figure[0].getY2());
		g.fillOval(figure[1].getX(), figure[1].getY(), figure[1].getR(), figure[1].getR()); 
		g.fillOval(figure[2].getX(), figure[2].getY(), figure[2].getR(), figure[2].getR()); 
		
		//for(int i=0; i<3; i++)
		//	g.drawLine(figure[3].line[i].getX1(),figure[3].line[i].getY1(), figure[3].line[i].getX2(), figure[3].line[i].getY2());
		//g.fillOval(figure[3].circular.getX(), figure[3].circular.getY(), figure[3].circular.getR(), figure[3].circular.getR()); 		
	}
	
	public static void main(String[] args) {
		
		for(int i=0; i<3; i++) line[i] = new Line(50*i, 100, 50*i, 150, 0);
		figure[0] = new Line(50, 10, 50, 60, 0); 
		System.out.println("servgdrg" + figure[0].getX1() + figure[0].getY1() + figure[0].getX2() + figure[0].getY2());
		figure[1] = new Circular(100, 100, 50, 0);
		figure[2] = new Circular(200, 200, 100, 0);
		figure[3] = new Composite(line, circular);
		for(int i=0; i<=3; i++) figure[i].draw();
		for(int i=0; i<=3; i++) figure[i].rotate(90);
		JFrame VFrame = new JFrame("Viet Anh - Shape Manager");
		VFrame.setSize(1006, 500);
		VFrame.setVisible(true);
		VFrame.setLayout(null);
		VFrame.setResizable(false);
		VFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Client VPanel = new Client();
		VPanel.setBackground(Color.GRAY);
		VPanel.setBounds(10, 10, 980, 400);
		VFrame.add(VPanel);
		VPanel.repaint();
		
	}

}

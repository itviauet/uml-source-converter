import java.awt.Graphics; 
import java.awt.Color; 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel; 
import javax.swing.JOptionPane;
import javax.swing.JPanel; 

import java.awt.Image;
public class Gomoku extends JPanel implements MouseListener, MouseMotionListener {
	
	private static final long serialVersionUID = 1L;
	public static int ROW = 16;
	public static int COLUMN = 16;
	public static int PIXELS = 20;
	public static final int WIDTH = COLUMN * PIXELS + COLUMN;
	public static final int HEIGHT = ROW * PIXELS + ROW;
	public static int NUMS_BOARD = 10;
	public static int NUMS_LINE = 10;
	
	public static boolean startGame = false;
	public static int level = -1;
	public static int mode = -1;

	public static int firstTurn = 0;
	
	public static final MyFrame OptionsFrame = new MyFrame("Options");
	public static final OptionsPanel optionsPanel = new OptionsPanel(OptionsFrame);
	
	public static Board board = new Board(ROW, COLUMN);
	public static int index = 0;

	public static JLabel infoLabel = new JLabel("CODED BY VIET ANH - K57CLC UET"); 
	
		
	public static final Gomoku PnlPlay = new Gomoku();
	
    public Gomoku() {
        super();
        addMouseListener(this);
        addMouseMotionListener(this);
       
        setVisible(true);
    }
   
    public static void playSound(String soundName) {
 	   try {
 	    AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(soundName).getAbsoluteFile());
 	    Clip clip = AudioSystem.getClip( );
 	    clip.open(audioInputStream);
 	    clip.start();
 	   } catch(Exception ex) {
 	     System.out.println("Cannot Play this Sound !");
 	     ex.printStackTrace( );
 	   }
 	 }	
    
    public static int swap(int index) {
    	if(index == 0) return 1;
    	return 0;
    }
    
    public static MainPanel PnlMain;
    
    public void playerClicked(int x, int y) {
    	
		board.grid[x][y] = index;
		board.empty[x][y] = false;
		String winner;
		
		repaint();
		if(board.gameOver()) {
			repaint();
			if(optionsPanel.snvalue) playSound("src/Sounds/Win.bin");
			if(index == 1) winner = "RED"; else winner = "BLUE";
			if(JOptionPane.showConfirmDialog(null, winner+" WIN !", "WINNER", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.YES_OPTION) {}
		
			startGame = false;
			return;
		} else 
		
		if(board.fullBoard()) {
			repaint();
			if(JOptionPane.showConfirmDialog(null, "NO ONE WIN THIS MATCH ", "WINNER", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.YES_OPTION) {}

			startGame = false;
			return;
		} 
    }

    public void computerClicked() {
    	int x = board.think(level, index).x;
		int y = board.think(level, index).y;
		board.grid[x][y] = index;
		board.empty[x][y] = false;
		if(board.gameOver()) {
			repaint();
			if(optionsPanel.snvalue) playSound("src/Sounds/Lost.bin");
			if(JOptionPane.showConfirmDialog(null, " COMPUTER WIN !", "WINNER", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.YES_OPTION) {}
			startGame = false;
			return;
		} else 
		if(board.fullBoard()) {
			repaint();
			if(JOptionPane.showConfirmDialog(null, "NO ONE WIN THIS MATCH ", "WINNER", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.YES_OPTION) {}
			startGame = false;
			return;
		}
    }
    
   
    public void mouseClicked(MouseEvent event) {
    	if(startGame) {
    		if (mode == 1) {
				int x = ((int)event.getX()-5)/(PIXELS+1)+1;
				int y = ((int)event.getY()-5)/(PIXELS+1)+1;
				if(board.empty[x][y] == true) {
					if(optionsPanel.snvalue) playSound("src/Sounds/Yes.bin");
					index = swap(index);
					playerClicked(x, y);	
					if(startGame) {
						index = swap(index);
						computerClicked();	
					}
				} else if(optionsPanel.snvalue) playSound("src/Sounds/No.bin");				
    		}
    		else
    		if(mode == 2) {
    			int x = ((int)event.getX()-5)/(PIXELS+1)+1;
				int y = ((int)event.getY()-5)/(PIXELS+1)+1;
				if(board.empty[x][y] == true) {
					if(optionsPanel.snvalue) playSound("src/Sounds/Yes.bin");
					index = swap(index);
					playerClicked(x, y);	
				} else if(optionsPanel.snvalue) playSound("src/Sounds/No.bin");
			
    		} else {
    			index = 1;
    			int x = board.think(level, index).x;
				int y = board.think(level, index).y;
				if(board.empty[x][y] == true) {
					index = swap(index);
					computerClicked();	
				}
				if(startGame) {
					index = swap(index);
					x = board.think(level, index).x;
					y = board.think(level, index).y;
					computerClicked();	
				}
    		}
    		repaint();		
    	} 
    }

    public void mousePressed(MouseEvent event) {}
    public void mouseReleased(MouseEvent event) {}
    public void mouseEntered(MouseEvent event) {}
    public void mouseExited(MouseEvent event) {}
    public void mouseDragged(MouseEvent event) {}
    public void mouseMoved(MouseEvent event) {}
    
    public static int bg = 2;
    public void nextBg() {
    	if(bg <= NUMS_BOARD-1) bg++;
    	else bg = 1;
    }
    

    public static int line = 5;
    public void nextLine() {
    	if(line <= NUMS_LINE-1) line++;
    	else line = 1;
    }
    
   
    public static ImageIcon imgbutton = new ImageIcon("src/Images/Ticks/1.png"); 
    
    public static ImageIcon img = new ImageIcon();
    public static ImageIcon tickIcon = new ImageIcon();
	private static JPanel pnlStatus2 = new JPanel();
    
	public static String themes = "Board";
	
	
	public void paint(Graphics g){
		
		super.paint(g);	

		if(optionsPanel.normalvalue) themes = "Board"; else
		if(optionsPanel.naturalvalue) themes = "Natural"; else
		if(optionsPanel.animalvalue) themes = "Animal"; else
		if(optionsPanel.cartoonvalue) themes = "Cartoon"; else
		if(optionsPanel.girlvalue) themes = "Zodiac"; 
		repaint();
		
		
		img = new ImageIcon("src/Images/Themes/" + themes + "/" + bg + ".bin");
		g.drawImage(img.getImage(), 0, 0, WIDTH+10, HEIGHT+10, null);
		setOpaque(false);
		
		if(line == 1) g.setColor( Color.WHITE); else
		if(line == 2) g.setColor( Color.PINK); else
		if(line == 3) g.setColor( Color.BLUE); else
		if(line == 4) g.setColor( Color.GREEN); else
		if(line == 5) g.setColor( Color.RED); else
		if(line == 6) g.setColor( Color.CYAN); else
		if(line == 7) g.setColor( Color.GRAY); else
		if(line == 8) g.setColor( Color.BLACK); else
		if(line == 9) g.setColor( Color.MAGENTA); else
		if(line == 10) g.setColor( Color.lightGray); 
		
		for(int i=0; i<=COLUMN; i++) g.drawLine(PIXELS*i+i+5, 0+5, PIXELS*i+i+5, HEIGHT+5);
		for(int i=0; i<=ROW; i++)	 g.drawLine(0+5, PIXELS*i+i+5, WIDTH+5, PIXELS*i+i+5);	
		
		for(int i=1; i<=ROW; i++)
			for(int j=1; j<=COLUMN; j++) 
				if(board.grid[i][j] != -1) {
					if(board.grid[i][j] == 2) {
						g.setColor(Color.WHITE);
						g.fillRect(2+(i-1)*(PIXELS+1)+5, 2+(j-1)*(PIXELS+1)+5, PIXELS-2, PIXELS-2);
						tickIcon = new ImageIcon("src/Images/Ticks/" + index + ".bin");
						g.drawImage(tickIcon.getImage(), 2+(i-1)*(PIXELS+1)+2+5, 2+(j-1)*(PIXELS+1)+2+5, PIXELS-6, PIXELS-6, null);
					}
					tickIcon = new ImageIcon("src/Images/Ticks/" + board.grid[i][j] + ".bin");
					g.drawImage(tickIcon.getImage(), 2+(i-1)*(PIXELS+1)+2+5, 2+(j-1)*(PIXELS+1)+2+5, PIXELS-6, PIXELS-6, null);
				}
	}
	
	public static void main(String[] args) {
		
		final MyFrame VFrame = new MyFrame("ViA-Gomoku");
		VFrame.setBounds(336, 100, 686, 538);
		VFrame.setVisible(true);
		
		
		ImageIcon icon = new ImageIcon("src/Images/Logo/Icon.bin");
		Image image = icon.getImage();
		VFrame.setIconImage(image); 
		
		PnlMain = new MainPanel();
		VFrame.add(PnlMain);
		PnlMain.setBounds(10, 10, 660, 490);
			
	
		VFrame.add(PnlPlay);
		PnlPlay.setBounds(164, 76, COLUMN*(PIXELS+1)+10, ROW*(PIXELS+1)+10);
		
		pnlStatus2 = new JPanel();
		pnlStatus2.repaint();
		
		PnlMain.loadBackGround = false;	

		OptionsFrame.setBounds(456, 179, 447, 410);
		OptionsFrame.setVisible(false);
		OptionsFrame.setLayout(null);
		OptionsFrame.setResizable(false);
		OptionsFrame.setDefaultCloseOperation(MyFrame.DO_NOTHING_ON_CLOSE);
		
		OptionsFrame.add(optionsPanel);
		
		JButton options = new JButton(new ImageIcon("src/Images/Button/Normal/Options.bin"));
		VFrame.add(options);
		options.setPressedIcon(new ImageIcon("src/Images/Button/Pressed/Options.bin"));
		options.setRolloverIcon(new ImageIcon("src/Images/Button/RollOver/Options.bin"));
		options.setBounds(550, HEIGHT-160, 102, 26);
		options.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(optionsPanel.snvalue) playSound("src/Sounds/Click.bin");
				OptionsFrame.setBounds(456, 179, 447, 410);
				OptionsFrame.setVisible(true);
			}
		}); 
		options.setBackground(Color.GREEN);
		options.setBorderPainted(false);

		final MyFrame StartFrame = new MyFrame();
		StartFrame.setUndecorated(true);
		StartFrame.setBounds(456, 179, 447, 410);
		StartFrame.setVisible(false);
		StartFrame.setLayout(null);
		StartFrame.setResizable(false);
		StartFrame.setDefaultCloseOperation(MyFrame.DO_NOTHING_ON_CLOSE);
		final StartPanel startPanel = new StartPanel(StartFrame, optionsPanel);
		StartFrame.add(startPanel);
		
		final JButton start = new JButton(new ImageIcon("src/Images/Button/Normal/Start.bin"));
		VFrame.add(start);
		start.setPressedIcon(new ImageIcon("src/Images/Button/Pressed/Start.bin"));
		start.setRolloverIcon(new ImageIcon("src/Images/Button/RollOver/Start.bin"));
		start.setBounds(550, HEIGHT-100, 102, 26);
		start.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(optionsPanel.snvalue) playSound("src/Sounds/Click.bin");
				StartFrame.setBounds(586, 300, 200, 110);
				StartFrame.setVisible(true);
				for(int i=1; i<=ROW; i++)
					for(int j=1; j<=COLUMN; j++)  {
						board.grid[i][j] = -1;
						board.empty[i][j] = true;
					}
				if(optionsPanel.comvalue) firstTurn = 0; else firstTurn = 1;
				if(mode == 1) {
					startPanel.firstTurn = firstTurn;
					if (firstTurn == 0) {
						board.grid[8][8] = index;
						board.empty[8][8] = false;
					} 	
				} else startPanel.firstTurn = 2;
				PnlPlay.repaint();			
				
				if(optionsPanel.mode1.getState()) mode = 1; else if(optionsPanel.mode2.getState()) mode = 2; else mode = 3;
				if(optionsPanel.level1.getState()) level = 1; else if(optionsPanel.level2.getState()) level = 2; else level = 3;
				startGame = true;
			}
		}); 
		start.setBackground(Color.GREEN);
		start.setBorderPainted(false);
		
		JButton restart = new JButton(new ImageIcon("src/Images/Button/Normal/Restart.bin"));
		VFrame.add(restart);
		restart.setPressedIcon(new ImageIcon("src/Images/Button/Pressed/Restart.bin"));
		restart.setRolloverIcon(new ImageIcon("src/Images/Button/RollOver/Restart.bin"));
		restart.setBounds(550, HEIGHT-130, 102, 26);
		restart.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(optionsPanel.snvalue) playSound("src/Sounds/Click.bin");
				if(JOptionPane.showConfirmDialog(null, "Restart Game will Delete all data ?", "Restart", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.YES_OPTION) {
					if(optionsPanel.mode1.getState()) mode = 1; else if(optionsPanel.mode2.getState()) mode = 2; else mode = 3;
					if(optionsPanel.level1.getState()) level = 1; else if(optionsPanel.level2.getState()) level = 2; else level = 3;
					StartFrame.setBounds(586, 300, 200, 110);
					StartFrame.setVisible(true);
					for(int i=1; i<=ROW; i++)
						for(int j=1; j<=COLUMN; j++)  {
							board.grid[i][j] = -1;
							board.empty[i][j] = true;
						}
					if(optionsPanel.comvalue) firstTurn = 0; else firstTurn = 1;
					if(mode == 1) {
						startPanel.firstTurn = firstTurn;
						if (firstTurn == 0) {
							board.grid[8][8] = index;
							board.empty[8][8] = false;
						} 	
					} else startPanel.firstTurn = 2;
					PnlPlay.repaint();			
					startGame = true;
					index = 0;			
					
		        }
			}
		}); 
		restart.setBackground(Color.GREEN);
		restart.setBorderPainted(false);
			
		final MyFrame CreditFrame = new MyFrame("Credits");
		CreditFrame.setBounds(456, 179, 447, 410);
		CreditFrame.setVisible(false);
		CreditFrame.setLayout(null);
		CreditFrame.setResizable(false);
		CreditFrame.setDefaultCloseOperation(MyFrame.DO_NOTHING_ON_CLOSE);
		final CreditPanel creditPanel = new CreditPanel(CreditFrame, optionsPanel);
		CreditFrame.add(creditPanel);

		JButton credit = new JButton(new ImageIcon("src/Images/Button/Normal/Credit.bin"));
		VFrame.add(credit);
		credit.setPressedIcon(new ImageIcon("src/Images/Button/Pressed/Credit.bin"));
		credit.setRolloverIcon(new ImageIcon("src/Images/Button/RollOver/Credit.bin"));
		credit.setBounds(550, HEIGHT-70, 102, 26);
		credit.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(optionsPanel.snvalue) playSound("src/Sounds/Click.bin");
				CreditFrame.setBounds(456, 179, 447, 410);
				CreditFrame.setVisible(true);		
			}
		}); 
		credit.setBackground(Color.GREEN);
		credit.setBorderPainted(false);
		
		JButton exit = new JButton(new ImageIcon("src/Images/Button/Normal/Exit.bin"));
		VFrame.add(exit);
		exit.setPressedIcon(new ImageIcon("src/Images/Button/Pressed/Exit.bin"));
		exit.setRolloverIcon(new ImageIcon("src/Images/Button/RollOver/Exit.bin"));
		exit.setBounds(550, HEIGHT-40, 102, 26);
		exit.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(optionsPanel.snvalue) playSound("src/Sounds/Click.bin");
				if(JOptionPane.showConfirmDialog(null, "Are you sure?", "Quit", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.YES_OPTION) {
		            System.exit(0);
		        }
			}
		}); 
		exit.setBackground(Color.GREEN);
		exit.setBorderPainted(false);
	    		
		JButton changeBackGround = new JButton("Boards");
		VFrame.add(changeBackGround);
		changeBackGround.setBounds(400, HEIGHT+125, 80, 20);
		changeBackGround.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(optionsPanel.snvalue) playSound("src/Sounds/Click.bin");
				PnlPlay.nextBg();
				PnlPlay.repaint();
			}
		});
		changeBackGround.setBackground(Color.GREEN);
		
		JButton changeLine = new JButton("Lines");
		VFrame.add(changeLine);
		changeLine.setBounds(315, HEIGHT+125, 80, 20);
		changeLine.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(optionsPanel.snvalue) playSound("src/Sounds/Click.bin");
				PnlPlay.nextLine();
				PnlPlay.repaint();
			}
		});
		changeLine.setBackground(Color.GREEN);
	}
	
}

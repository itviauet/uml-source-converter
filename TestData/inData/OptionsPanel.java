import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
class OptionsPanel extends JPanel {
	
	// options panel, chua tat ca cac cai dat trong game
	
	private static final long serialVersionUID = 1L;
	public PlaySound sound = new PlaySound();
	public ImageIcon background = new ImageIcon("src/Images/Dialog/MainOptions.bin");
	final JButton okButton = new JButton(new ImageIcon("src/Images/Button/Normal/Ok.bin"));
	final JButton cancelButton = new JButton(new ImageIcon("src/Images/Button/Normal/Cancel.bin"));
	
	public MyFrame InfoFrame = new MyFrame();
	
	// lua chon che do choi
	CheckboxGroup checkGroup = new CheckboxGroup();
    final Checkbox mode1=new Checkbox("", checkGroup,true);
    final Checkbox mode2=new Checkbox("",checkGroup,false);
    final Checkbox mode3=new Checkbox("",checkGroup,false);
    // lua chon difficulty
    CheckboxGroup levelGroup = new CheckboxGroup();
    final Checkbox level1=new Checkbox("",levelGroup,false);
    final Checkbox level2=new Checkbox("",levelGroup,false);
    final Checkbox level3=new Checkbox("",levelGroup,true);
	// bat nhac nen
    final Checkbox ms=new Checkbox("", true);
    // bat nhac khi click
    final Checkbox sn=new Checkbox("", true);
    // thay doi luot danh , cai dat cho minh danh truoc hay may tinh danh truoc
    CheckboxGroup turnGroup =new CheckboxGroup();
    final Checkbox com=new Checkbox("",turnGroup,true);
    final Checkbox you=new Checkbox("",turnGroup,true);
    
    // cai dat hinh nen cho board
    CheckboxGroup themes = new CheckboxGroup();
    final Checkbox normal=new Checkbox("", themes,true);
    final Checkbox natural=new Checkbox("",themes,false);
    final Checkbox animal=new Checkbox("",themes,false);
    final Checkbox cartoon=new Checkbox("", themes,false);
    final Checkbox girl=new Checkbox("",themes,false);
    
    public static PlaySound playMusic = new PlaySound();
     // cac bien luu gia tri tam thoi , de neu an nut cancel thi backup lai
    public boolean mode1value = true;
    public boolean mode2value = false;
    public boolean mode3value = false;
    public boolean level1value = false;
    public boolean level2value = false;
    public boolean level3value = true;
    public boolean msvalue = true;
    public boolean snvalue = true;
    public boolean comvalue = false;
    public boolean youvalue = true;
    public boolean normalvalue = true;
    public boolean naturalvalue = false;
    public boolean animalvalue = false;
    public boolean cartoonvalue = false;
    public boolean girlvalue = false;
    
    public boolean playingMusic = true;
    
    public OptionsPanel() {
	}
	public OptionsPanel(MyFrame infoFrame) {
		
		playMusic.start();
		this.InfoFrame = infoFrame;
		setLayout(null);
		setBounds(0, 0, 440, 380);
		
		// add va setbounds cho cac doi tuong
		
		add(okButton);
		add(cancelButton);
		
	    add(mode1);
	    add(mode2);
	    add(mode3);
	    mode1.setBounds(52, 92, 12, 12);
	    mode2.setBounds(52, 112, 12, 12);
	    mode3.setBounds(52, 132, 12, 12);
	    mode1.setBackground(Color.PINK);
	    mode2.setBackground(Color.PINK);
	    mode3.setBackground(Color.PINK);
	    
	    add(level1);
	    add(level2);
	    add(level3);
	    level1.setBounds(296, 92, 12, 12);
	    level2.setBounds(296, 112, 12, 12);
	    level3.setBounds(296, 132, 12, 12);
	    level1.setBackground(Color.PINK);
	    level2.setBackground(Color.PINK);
	    level3.setBackground(Color.PINK);
	    
	    add(com);
	    add(you);
	    com.setBounds(295, 185, 12, 12);
	    com.setBackground(Color.PINK);
	    you.setBounds(295, 205, 12, 12);
	    you.setBackground(Color.PINK);
	    
		ms.setBounds(295, 250, 12, 12);
	    ms.setBackground(Color.PINK);
	    add(ms);
	    
	    sn.setBounds(350, 250, 12, 12);
	    sn.setBackground(Color.PINK);
	    add(sn);
	    
	    add(normal);
	    add(natural);
	    add(animal);
	    add(cartoon);
	    add(girl);
	    normal.setBounds(52, 185, 12, 12);
	    natural.setBounds(52, 202, 12, 12);
	    animal.setBounds(52, 222, 12, 12);
	    cartoon.setBounds(52, 242, 12, 12);
	    girl.setBounds(52, 262, 12, 12);
	    normal.setBackground(Color.PINK);
	    natural.setBackground(Color.PINK);
	    animal.setBackground(Color.PINK);
	    cartoon.setBackground(Color.PINK);
	    girl.setBackground(Color.PINK);
	
	}
	public void applyState() {
		mode1value = mode1.getState();
		mode2value = mode2.getState();
		mode3value = mode3.getState();
		level1value = level1.getState();
		level2value = level2.getState();
		level3value = level3.getState();
		msvalue = ms.getState();
		snvalue = sn.getState();
		comvalue = com.getState();
		youvalue = you.getState();
		normalvalue = normal.getState();
		naturalvalue = natural.getState();
		animalvalue = animal.getState();
		cartoonvalue = cartoon.getState();
		girlvalue = you.getState();
	}
	public void cancelState() {
		mode1.setState(mode1value);
		mode2.setState(mode2value);
		mode3.setState(mode3value);
		level1.setState(level1value);
		level2.setState(level2value);
		level3.setState(level3value);
		ms.setState(msvalue);
		sn.setState(snvalue);
		com.setState(comvalue);
		you.setState(youvalue);
		normal.setState(normalvalue);
		natural.setState(naturalvalue);
		animal.setState(animalvalue);
		cartoon.setState(cartoonvalue);
		girl.setState(girlvalue);
		
	}
	
	@SuppressWarnings("deprecation")
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.GREEN);
		g.fillRect(0, 0, 440, 380);
		g.drawImage(background.getImage(), 0, 0, 440, 380, null);
		
		okButton.setBounds(120, 300, 70, 27);
		okButton.setBackground(Color.PINK);
		okButton.setBorderPainted(false);
		okButton.setPressedIcon(new ImageIcon("src/Images/Button/Pressed/Ok.bin"));
		okButton.setRolloverIcon(new ImageIcon("src/Images/Button/Rollover/Ok.bin"));
		// set su kien cho nut ok
		okButton.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				InfoFrame.setVisible(false);
				applyState();
				if(snvalue) sound.playSound("src/Sounds/Click.bin");
				
				if(msvalue && !playingMusic) {
				//	PlaySound playMusic = new PlaySound(); 
				//	playMusic.start();
				}
				
				// cho nay de tat nhac nen nhung tai sao no ko chay e cung ko hieu
				// e da stop thread chay nhac nhung no van chay @@, vi the nen ham bat nhac tro lai ben tren ko dung dc, 
				// neu dung nhac se bi lap lai
				
				if(!msvalue && playMusic.isAlive() ) {
					playMusic.stop();
					playingMusic = false;
					System.out.println("stop music");
				}
			}
		});
	// set su kien cho nut cancel
		cancelButton.setBounds(250, 300, 70, 27);
		cancelButton.setBackground(Color.PINK);
		cancelButton.setBorderPainted(false);
		cancelButton.setPressedIcon(new ImageIcon("src/Images/Button/Pressed/Cancel.bin"));
		cancelButton.setRolloverIcon(new ImageIcon("src/Images/Button/Rollover/Cancel.bin"));
		cancelButton.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				InfoFrame.setVisible(false);
				cancelState();
				if(snvalue) sound.playSound("src/Sounds/Click.bin");
			}
		}); 
	}
}
import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

// Main panel
class MainPanel extends JPanel{
	public boolean loadBackGround = true;
	private static final long serialVersionUID = 1L;
	public ImageIcon background = new ImageIcon("src/Images/Background/Background.bin");
	public ImageIcon logo = new ImageIcon("src/Images/Logo/Gomoku.bin");
	public static String[] statusLabel = {"src/Images/Status/StatusLabel.bin", 
		  "src/Images/Status/GameStarted.bin", 
		  "src/Images/Status/TurnRed.bin", 
		  "src/Images/Status/TurnBlue.bin", 
		  "src/Images/Status/RedWin.bin", 
		  "src/Images/Status/BlueWin.bin", 
		  "src/Images/Status/NooneWin.bin" } ;
	public Gomoku gomoku = new Gomoku();
	public int status = 0;
	// Ham paint cua main panel, ve hinh nen cho game
	public void paint(Graphics g){
		if(loadBackGround) {
			super.paint(g);
			g.drawImage(background.getImage(), 0, 0, 660, 490, null);
		}
		ImageIcon statusIcon = new ImageIcon(statusLabel[status]);
		g.drawImage(statusIcon.getImage(), 400, 37, 100, 25, null);
	}
	// constructor
	public MainPanel() {
        super();
        setVisible(true);
    }
}
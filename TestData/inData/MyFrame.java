import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import javax.swing.JFrame;
class MyFrame extends JFrame {
	//  tuy bien dua tren JFrame
	private static final long serialVersionUID = 1L;
	Toolkit toolkit = Toolkit.getDefaultToolkit();
    Image image = toolkit.getImage("src/Images/Ticks/Mouse.bin");
    Point hotspot = new Point(0, 0);
    Cursor cursor = toolkit.createCustomCursor(image, hotspot, "Stone");
    public void init() {
    	setLayout(null);
		setResizable(false);
		setDefaultCloseOperation(MyFrame.EXIT_ON_CLOSE);
		setCursor(cursor);
    }
	public MyFrame(String s) {
		super(s);
		init();
	}
	public MyFrame(){
		init();
	}
}

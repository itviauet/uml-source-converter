import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
public class CreditPanel extends JPanel {
	//  Credit Panel
	private static final long serialVersionUID = 1L;
	public PlaySound sound = new PlaySound();
	public ImageIcon background = new ImageIcon("src/Images/Dialog/MainCredit.bin");
	final JButton okButton = new JButton(new ImageIcon("src/Images/Button/Normal/Ok.bin"));
	public MyFrame CreditFrame = new MyFrame();
	public OptionsPanel options = new OptionsPanel();
	public CreditPanel(MyFrame CreditFrame, OptionsPanel options) {
		this.CreditFrame = CreditFrame;
		setLayout(null);
		setBounds(0, 0, 440, 380);
		this.options = options;
	}
	// ham paint cua panel
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.GREEN);
		g.fillRect(0, 0, 443, 380);
		// ve hinh nen
		g.drawImage(background.getImage(), 0, 0, 440, 380, null);
		// add button cho frame
		add(okButton);
		okButton.setBounds(185, 295, 70, 27);
		okButton.setBackground(Color.PINK);
		okButton.setBorderPainted(false);
		okButton.setPressedIcon(new ImageIcon("src/Images/Button/Pressed/Ok.bin"));
		okButton.setRolloverIcon(new ImageIcon("src/Images/Button/Rollover/Ok.bin"));
		okButton.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(options.snvalue) sound.playSound("src/Sounds/Click.bin");
				CreditFrame.setVisible(false);
			}
		}); 
	}
}
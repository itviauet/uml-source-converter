import java.io.File;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

// tao 1 thread de phat nhac
class PlaySound extends Thread {
	PlaySound() {
	}
    public void playSound(String soundName) {
  	   try {
  	    AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(soundName).getAbsoluteFile());
  	    Clip clip = AudioSystem.getClip( );
  	    clip.open(audioInputStream);
  	    clip.start();
  	   } catch(Exception ex) {
  	     System.out.println("CANNOT PLAY SOUND");
  	     ex.printStackTrace( );
  	   }
  	 }	
    // ham run()
	public void run() {
		for(int i=1; i<=10000; i++) {
			playSound("src/Sounds/Music.bin");
			try { Thread.sleep(60000);} 
			catch(Exception ex) {
				ex.printStackTrace();
			}   
		}
	}
}
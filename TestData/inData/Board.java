
// class luu bang de tinh nuoc di cua may tinh va nguoi choi 
// Mang e dung tu 1 -> n vi e de ra hang so 0 lam khung vien xung quanh, tien de su dung cac thuat toan ve sau

/*Gomoku extends JPanel imple
ments MouseListener, MouseMotionListener { */ 
public class Board {
	public int ROW, COLUMN;
	public int[][] grid = new int[200][200]; // gia tri tai o do
	public boolean[][] empty = new boolean[200][200]; // xem xem tai o do co trong hay ko
	Board(int row, int column) {
		this.ROW = row;
		this.COLUMN = column;
		for(int i=0; i<=row+1; i++)
			for(int j=0; j<=column+1; j++)  {
				grid[i][j] = -1;
				empty[i][j] = true;
			}
	}
	public boolean fullBoard() {
		for(int i=1; i<ROW; i++)
			for(int j=1; j<=COLUMN; j++)
				if(grid[i][j] == -1)
					return false;
		if(!gameOver()) return true;
		return false;
	}
	// kiem tra 1 diem co don doc hay ko, tra ve true neu xung quanh no co quan khac
	public boolean notFA(int i, int j) {
		if(empty[i-1][j-1] || empty[i-1][j] || empty[i-1][j+1] || empty[i][j-1] || empty[i][j+1] || empty[i+1][j-1] || empty[i+1][j] || empty[i+1][j+1]  ) return true;
		return false;
	}
	
	// ham kiem tra xem toa do i, j co an duoc luon ko, an duoc thi an luon
	public boolean anLuonVaNgay4(int i, int j, int index) {
		if(!empty[i][j]) return false;
		int count;
		
		count = 0;
		for(int k=1; k<=4; k++) {
			if(j+k <= COLUMN) {
				if(grid[i][j+k] == index) count++;
			}
		}
		if(count >=4) return true;
		
		count = 0;
		for(int k=1; k<=4; k++) {
			if(j-k >= 1) {
				if(grid[i][j-k] == index) count++;
			}
		}
		if(count >=4)  return true;
		
		count = 0;
		for(int k=1; k<=4; k++) {
			if(i-k >= 1) {
				if(grid[i-k][j] == index) count++;
			}
		}
		if(count >=4) return true;
		
		count = 0;
		for(int k=1; k<=4; k++) {
			if(i+k <= ROW) {
				if(grid[i+k][j] == index) count++;
			}
		}
		if(count >=4)  return true;
		
		count = 0;
		for(int k=1; k<=4; k++) {
			if(i-k >= 1 && j-k >= 1) {
				if(grid[i-k][j-k] == index) count++;
			}
		}
		if(count >=4)  return true;
		
		count = 0;
		for(int k=1; k<=4; k++) {
			if(i+k <= COLUMN && j+k <= COLUMN) {
				if(grid[i+k][j+k] == index) count++;
			}
		}
		if(count >=4)  return true;
		
		count = 0;
		for(int k=1; k<=4; k++) {
			if(i-k >= 1 && j+k <= COLUMN) {
				if(grid[i-k][j+k] == index) count++;
			}
		}
		if(count >=4) return true;
		count = 0;
		for(int k=1; k<=4; k++) {
			if(i+k <= ROW && j-k >= 1) {
				if(grid[i+k][j-k] == index) count++;
			}
		}
		if(count >=4)  return true;
		
		return false;
	}
	
	// ham chan nuoc co doi cua doi thu, tuy nhien e da disable ham nay vi cac ham khac no chua toi uu nen ham nay se anh huong
	// ket qua tinh toan cua cac ham khac
	public boolean chanNuocDoi(int i, int j, int index) {
		int k;
		int count = 0;
		int sum = 0;
		int index2 = index;
		if(index == 0) index2 = 1; else index2 = 0;
		
		k = 1;
		while(empty[i][j+k] && j+k <= ROW ) k++;
		while(grid[i][j+k] == index2 && j+k <= ROW && k<=4) {
			count++;
			k++;
		}
		if(count >= 2) sum++;
		
		count = 0;
		k = 1;
		while(empty[i][j-k] && j-k >= 1) k++;
		while(grid[i][j-k] == index2 && j-k >=0 && k<=4) {
			count++;
			k++;
		}
		if(count >= 2) sum++;
		
		count = 0;
		k = 1;
		while(empty[i-k][j] && i-k >= 1) k++;
		while(grid[i-k][j] == index2 && i-k >=0 && k<=4) {
			count++;
			k++;
		}
		if(count >= 2) sum++;
		
		count = 0;
		k = 1;
		while(empty[i+k][j] && i+k <= COLUMN ) k++;
		while(grid[i+k][j] == index2 && i+k <= COLUMN && k<=4) {
			count++;
			k++;
		}
		if(count >= 2) sum++;
		
		k = 1;
		while(empty[i+k][j+k] && j+k <= ROW && i+k <= COLUMN) k++;
		while(grid[i+k][j+k] == index2 && j+k <= ROW && i+k <= COLUMN && k<=4) {
			count++;
			k++;
		}
		if(count >= 2) sum++;
		
		count = 0;
		k = 1;
		while(empty[i-k][j-k] && j-k >= 1 && i-k >=1 ) k++;
		while(grid[i-k][j-k] == index2 && j-k >=1 && i-k >= 1 && k<=4) {
			count++;
			k++;
		}
		if(count >= 2) sum++;
		
		count = 0;
		k = 1;
		while(empty[i-k][j+k] && i-k >= 1 && j+k <= ROW) k++;
		while(grid[i-k][j+k] == index2 && i-k >=1 && j+k <= ROW && k<=4) {
			count++;
			k++;
		}
		if(count >= 2) sum++;
		
		count = 0;
		k = 1;
		while(empty[i+k][j-k]&& i+k <= COLUMN && j-k >=1 ) k++;
		while(grid[i+k][j-k] == index2 && i+k <= COLUMN && j-k >= 1 && k<=4) {
			count++;
			k++;
		}
		if(count >= 2) sum++;
		
		if(sum >= 2) { 
			System.out.println(i + ";" +  j);
			return true;
		}
		return false;
	}
	
	// kiem tra neu doi phuong co 4 o lien tiep nhung co 1 khoang cach o giua
	public boolean chanOGiua(int i, int j, int index) {
		int index2;
		if(index == 1) index2 = 0; else index2 = 1;
		int count = 0;
		int k;
		
		k=1;
		while(grid[i][j+k] == index2 && j+k <= ROW) { count++; k++; }
		k=1;
		while(grid[i][j-k] == index2 && j-k >=1)  { count++; k++; }
		if(count >=3 ) return true;
		
		count = 0;
		k=1;
		while(grid[i+k][j] == index2 && i+k <= COLUMN)  { count++; k++; }
		k=1;
		while(grid[i-k][j] == index2 && i-k >=1)  { count++; k++; }
		if(count >=3 ) return true;
		
		
		count = 0;
		k=1;
		while(grid[i+k][j+k] == index2 && j+k <= ROW && i+k <= COLUMN)  { count++; k++; }
		k=1;
		while(grid[i-k][j-k] == index2 && j-k >=1 && i-k >= 1 )  { count++; k++; }
		if(count >=3 ) return true;
		
		count = 0;
		
		k=1;
		while(grid[i-k][j+k] == index2 && j+k <= ROW && i-k >= 1)  { count++; k++; }
		k=1;
		while(grid[i+k][j-k] == index2 && j-k >=1 && i+k <= COLUMN )  { count++; k++; }
		if(count >=3 ) return true;
		
		return false;
	}
	
	// kiem tra can phai danh ngay tai o i, j voi index la chi so cua ng choi va number la so luong quan minh muon tim
	// vi duj 4 la danh ngay khi co 4 quan lien tiep cua doi thu, 3 la 3 quan
	public boolean needToMark(int i, int j, int index, int number) {
		if(!empty[i][j]) return false;
		int[] save = new int[10];
		int count;
		if(index == 0) index = 1; else if (index == 1) index = 0;
		
		count = 0;
		for(int k=1; k<=number; k++) {
			if(j+k <= COLUMN) {
				save[k] = grid[i][j+k];
				if(save[k] == index) count++;
			}
		}
		if(count >=number) return true;
		
		
		count = 0;
		for(int k=1; k<=number; k++) {
			if(j-k >= 1) {
				save[k] = grid[i][j-k];
				if(save[k] == index) count++;
			}
		}
		if(count >=number) return true;
		
		count = 0;
		for(int k=1; k<=number; k++) {
			if(i-k >= 1) {
				save[k] = grid[i-k][j];
				if(save[k] == index) count++;
			}
		}
		if(count >=number) return true;
		
		count = 0;
		for(int k=1; k<=number; k++) {
			if(i+k <= ROW) {
				save[k] = grid[i+k][j];
				if(save[k] == index) count++;
			}
		}
		if(count >=number) return true;
		
		count = 0;
		for(int k=1; k<=number; k++) {
			if(i-k >= 1 && j-k >= 1) {
				save[k] = grid[i-k][j-k];
				if(save[k] == index) count++;
			}
		}
		if(count >=number) return true;
		
		count = 0;
		for(int k=1; k<=number; k++) {
			if(i+k <= COLUMN && j+k <= COLUMN) {
				save[k] = grid[i+k][j+k];
				if(save[k] == index) count++;
			}
		}
		if(count >=number) return true;
		
		count = 0;
		for(int k=1; k<=number; k++) {
			if(i-k >= 1 && j+k <= COLUMN) {
				save[k] = grid[i-k][j+k];
				if(save[k] == index) count++;
			}
		}
		if(count >=number) return true;
		
		count = 0;
		for(int k=1; k<=number; k++) {
			if(i+k <= ROW && j-k >= 1) {
				save[k] = grid[i+k][j-k];
				if(save[k] == index) count++;
			}
		}
		if(count >=number) return true;
		
		return false;
	}
	
	// ham may tinh nghi nuoc co, tra ve toa do cua i, j
	public Point think(int level, int index) {
		
		int index2 = 0;
		if(index == 0) index2 = 1; else if (index == 1) index2 = 0;
		int x = 8, y = 8;
		for(int i=2; i<=ROW-1; i++)
			for(int j=2; j<=COLUMN-1; j++)
				if(grid[i][j] == index2 ) {
					int ran = (int)(8*Math.random());
					if(ran == 0 && grid[i-1][j-1] == -1) { x = i-1; y = j-1;} else
					if(ran == 1 && grid[i-1][j] == -1) { x = i-1; y = j; 	} else
					if(ran == 2 && grid[i-1][j+1]  == -1) { x = i-1; y = j+1; } else
					if(ran == 3 && grid[i][j-1]  == -1) { x = i; y = j-1; } else
					if(ran == 4 && grid[i][j+1]  == -1) { x = i; y = j+1; } else
					if(ran == 5 && grid[i+1][j-1]  == -1) { x = i+1; y = j-1; } else
					if(ran == 6 && grid[i+1][j]  == -1) { x = i+1; y = j; } else
					if(ran == 7 && grid[i+1][j+1]  == -1) { x = i+1; y = j+1; }
					break;
				}
		
		
		if (level == 3) {
			int[][] save = new int[ROW+1][COLUMN+1];
			for(int i=1; i<=ROW; i++)
				for(int j=1; j<=COLUMN; j++) 
				if(empty[i][j]) {
					if(anLuonVaNgay4(i, j, index) && empty[i][j]) return new Point(i, j);
					//if(chanNuocDoi(i, j, index) && empty[i][j]) return new Point(i, j);
					if(needToMark(i, j, index, 4) && empty[i][j]) return new Point(i, j);
					if(chanOGiua(i, j, index) && empty[i][j]) return new Point(i, j); 
					if(needToMark(i, j, index, 3) && empty[i][j]) return new Point(i, j);
					if(empty[i][j] == true) {
						int k = j+1;
						save[i][j] = 0;
						while(k <= ROW && index == grid[i][k] && grid[i][k] != -1) { 
							save[i][j]++;
							k++;
						}
						k = i+1;
						while(k <= COLUMN && index == grid[k][j] && grid[k][j] != -1) { 
							save[i][j]++;
							k++;
						}
						k = 1;
						while(k <= ROW-j && k <= COLUMN-i && index == grid[i+k][j+k] && grid[i+k][j+k] != -1){
							save[i][j]++;
							k++;
						}
						k = 1;
						while(i-k>= 0 && j-k>=0 && index == grid[i-k][j-k] && grid[i-k][j-k] != -1 ){
							save[i][j]++;
							k++;
						}
						k = 1;
						while(i+k<=COLUMN && j-k>=0 && index == grid[i+k][j-k] && grid[i+k][j-k] != -1){
							save[i][j]++;
							k++;
						}
						k = 1;
						while(i-k>=0 && j+k<=ROW && index == grid[i-k][j+k] && grid[i-k][j+k] != -1){
							save[i][j]++;
							k++;
						}
					}
				}
			
			int max = 0;
			for(int i=1; i<=ROW; i++)
				for(int j=1; j<=COLUMN; j++)
					if(save[i][j] > max) {
						max = save[i][j];
						x = i;
						y = j;
					}
		} else 
		if (level == 2) {
			int index3 = 0;
			if (index == 1) index = 0; else if (index == 0) index = 1;
			if (index == 1) index = 3; else if (index == 0) index = 3;
			int[][] save = new int[ROW+1][COLUMN+1];
			for(int i=1; i<=ROW; i++)
				for(int j=1; j<=COLUMN; j++) 
				if(empty[i][j]){
					if(anLuonVaNgay4(i, j, index) && empty[i][j]) return new Point(i, j);
					//if(chanNuocDoi(i, j, index) && empty[i][j]) return new Point(i, j);
					if(needToMark(i, j, index3,4) && empty[i][j]) return new Point(i, j);
					if(chanOGiua(i, j, index) && empty[i][j]) return new Point(i, j); 
					if(needToMark(i, j, index3, 3) && empty[i][j]) return new Point(i, j);
					if(empty[i][j] == true) {
						int k = j+1;
						save[i][j] = 0;
						while(k <= ROW && index == grid[i][k] && grid[i][k] != -1) { 
							save[i][j]++;
							k++;
						}
						k = i+1;
						while(k <= COLUMN && index == grid[k][j] && grid[k][j] != -1) { 
							save[i][j]++;
							k++;
						}
						k = 1;
						while(k <= ROW-j && k <= COLUMN-i && index == grid[i+k][j+k] && grid[i+k][j+k] != -1){
							save[i][j]++;
							k++;
						}
						k = 1;
						while(i-k>= 0 && j-k>=0 && index == grid[i-k][j-k] && grid[i-k][j-k] != -1 ){
							save[i][j]++;
							k++;
						}
						k = 1;
						while(i+k<=COLUMN && j-k>=0 && index == grid[i+k][j-k] && grid[i+k][j-k] != -1){
							save[i][j]++;
							k++;
						}
						k = 1;
						while(i-k>=0 && j+k<=ROW && index == grid[i-k][j+k] && grid[i-k][j+k] != -1){
							save[i][j]++;
							k++;
						}
					}
				}
			
			int max = 0;
			for(int i=1; i<=ROW; i++)
				for(int j=1; j<=COLUMN; j++)
					if(save[i][j] > max) {
						max = save[i][j];
						x = i;
						y = j;
					}
		} else {
			int index3 = 0;
			if(index == 0) index3 = 1;
			for(int i=1; i<=ROW; i++)
				for(int j=1; j<=COLUMN; j++) 
					if(empty[i][j]){
					if(anLuonVaNgay4(i, j, index) && empty[i][j]) return new Point(i, j);
					//if(chanNuocDoi(i, j, index) && empty[i][j]) return new Point(i, j);
					if(needToMark(i, j, index, 4) && empty[i][j]) return new Point(i, j);
					if(chanOGiua(i, j, index) && empty[i][j]) return new Point(i, j);
					if(needToMark(i, j, index, 3) && empty[i][j]) return new Point(i, j);
					if(grid[i][j] == index3 && notFA(i, j)) {
						if(empty[i-1][j-1]) { x = i-1; y = j-1;} else
						if(empty[i-1][j]) { x = i-1; y = j; 	} else
						if(empty[i-1][j]) { x = i-1; y = j+1; } else
						if(empty[i][j-1]) { x = i; y = j-1; } else
						if(empty[i][j+1]) { x = i; y = j+1; } else
						if(empty[i+1][j-1]) { x = i+1; y = j-1; } else
						if(empty[i+1][j]) { x = i+1; y = j; } else
						if(empty[i+1][j+1]) { x = i+1; y = j+1; }
						break;
					}
				}
		}
		
		return new Point(x, y);
	}
	
	// kiem tra 1 diem co phai la 1 phan tu trong day nuoc co chien thang ko
	public boolean over(int i, int j) {
		if (grid[i][j] != -1) {
			int k = j;
			int count = 0;
			while(k <= ROW && grid[i][j] == grid[i][k]) { 
				count++;
				k++;
				if(count>=5) {
					for(int l=j; l<=j+4; l++) grid[i][l] = 2;
					return true;
				}
			}
			k = i;
			count = 0;
			while(k <= COLUMN && grid[i][j] == grid[k][j]) { 
				count++;
				k++;
				if(count>=5)  {
					for(int l=i; l<=i+4; l++) grid[l][j] = 2;
					return true;
				}
			}
			k = 0;
			count = 0;
			while(k <= ROW-j && k <= COLUMN-i && grid[i][j] == grid[i+k][j+k]){
				count++;
				k++;
				if(count>=5)  {
					k = 0;
					while(k <= 4) {
						grid[i+k][j+k] = 2;
						k++;
					}
					return true;
				}
			}
			k = 0;
			count = 0;
			while(i-k>= 0 && j-k>=0 && grid[i][j] == grid[i-k][j-k]){
				count++;
				k++;
				if(count>=5)  {
					k = 0;
					while(k <= 4) {
						grid[i-k][j-k] = 2;
						k++;
					}
					return true;
				}
			}
			k = 0;
			count = 0;
			while(i+k<=COLUMN && j-k>=0 && grid[i][j] == grid[i+k][j-k]){
				count++;
				k++;
				if(count>=5)  {
					k = 0;
					while(k <= 4) {
						grid[i+k][j-k] = 2;
						k++;
					}
					return true;
				}
			}
			k = 0;
			count = 0;
			while(i-k>=0 && j+k<=ROW && grid[i][j] == grid[i-k][j+k]){
				count++;
				k++;
				if(count>=5)  {
					k = 0;
					while(k <= 4) {
						grid[i-k][j+k] = 2;
						k++;
					}
					return true;
				}
			}
			return false;
		}
		return false;
	}
	// game over, tra ve true neu game ket thuc
	public boolean gameOver() {
		for(int i=1; i<=ROW; i++)
			for(int j=1;j<=COLUMN; j++) {
				if(grid[i][j] != -1) {
					if(over(i, j)) return true;
				}
			}
		return false;
	}
}

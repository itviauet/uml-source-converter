import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class StartPanel extends JPanel {
	// Panel start so ra khi an nut start bat dau game
	private static final long serialVersionUID = 1L;
	public PlaySound sound = new PlaySound();
	public ImageIcon background = new ImageIcon("src/Images/Dialog/MainStart.bin");
	final JButton okButton = new JButton(new ImageIcon("src/Images/Button/Normal/Ok.bin"));
	public MyFrame StartFrame = new MyFrame();
	public OptionsPanel options = new OptionsPanel();
	public StartPanel(MyFrame CreditFrame, OptionsPanel options) {
		this.StartFrame = CreditFrame;
		setLayout(null);
		setBounds(0, 0, 440, 380);
		this.options = options;
	}
	public static JLabel label = new JLabel("Welcome");
	public int firstTurn = 1;
	// Ham paint cua class StartPanel, ve hinh vao frame StartPanel
	public void paint(Graphics g) {
		super.paint(g);
		add(okButton);
		g.setColor(Color.green);
		g.fillRect(0, 0, 200, 75);
		g.drawImage(background.getImage(), 20, 0, 180, 80, null);
		okButton.setBounds(3, 80, 70, 27);
		okButton.setBackground(Color.PINK);
		okButton.setBorderPainted(false);
		okButton.setPressedIcon(new ImageIcon("src/Images/Button/Pressed/Ok.bin"));
		okButton.setRolloverIcon(new ImageIcon("src/Images/Button/Rollover/Ok.bin"));
		okButton.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(options.snvalue) sound.playSound("src/Sounds/Click.bin");
				StartFrame.setVisible(false);
			}
		}); 
		add(label);
		if(firstTurn == 0) label.setText("Computer first !");
		else if(firstTurn == 1) label.setText("     You first !");
		else if(firstTurn == 2) label.setText("     Start Game !");
		label.setBounds(90, 80, 100, 27);
	}
}
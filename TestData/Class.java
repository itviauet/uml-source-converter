import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

public class Class {
	public String name;
	public boolean isAbstract;
	public String isExtends;
	public Vector<String> isImplements;
	public Vector<String> variable;
	public Vector<String> function;
	public Vector<String> has_a;
	Class() {
		name = "";
		isAbstract = false;
		isExtends = "Object";
		isImplements = new Vector<String>();
		variable = new Vector<String>();
		function = new Vector<String>();
		has_a = new Vector<String>();
	}
	public void print(FileWriter writer) {
		try {	
			writer.write("\n\n - CLASS: " + name + "\n");
			writer.write("\t + ABSTRACT: ");
			if(isAbstract) writer.write("YES\n"); else writer.write("NO\n");
			writer.write("\t + IMPLEMENTS: ");
			if(isImplements.size() == 0) writer.write("NO");
			else
			for(int i=0; i<isImplements.size(); i++)
				writer.write(isImplements.get(i) + " ");
			writer.write("\n");
			writer.write("\t + EXTENDS: " + isExtends + "\n");
			writer.write("\t + HAS: ");
			for(int i=0; i<has_a.size(); i++) writer.write(has_a.get(i)+ " ");
			writer.write("\n");
			writer.write("\t + VARIABLES: ");
			for(int i=0; i<variable.size(); i++) writer.write("\n\t\t" + variable.get(i));
			writer.write("\n\t + METHODS: ");
			for(int i=0; i<function.size(); i++) writer.write("\n\t\t" + function.get(i));
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
	}
}